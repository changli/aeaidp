package com.agileai.hotweb.bizmoduler.core;

import java.util.List;
import java.util.Map;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.domain.TreeModel;

public interface TreeAndContentManage extends TreeSelectService,TreeStructureManage{
	public static final String BASE_TAB_ID = "_base_";
	public static final String TAB_ID = "_tabId_";
	public static final String TAB_INDEX = "_tabIndex_";
	
	void deletContentRecord(String tabId,DataParam param);
	void createtContentRecord(String tabId,DataParam param);
	void createtContentRecord(String tabId,DataParam param,String primaryKey);
	void updatetContentRecord(String tabId,DataParam param);
	DataRow getContentRecord(String tabId,DataParam param);
	List<DataRow> findContentRecords(TreeModel treeModel,String tabId,DataParam param);
	
	void insertTreeContentRelation(String tabId,DataParam param);
	void updateTreeContentRelation(String tabId,DataParam param);
	boolean isLastTreeContentRelation(String tabId,DataParam param);
	void removeTreeContentRelation(String tabId,DataParam param);
	
	List<DataRow> findTreeRecords(DataParam param);
	
	Map<String,String> getTabIdAndColFieldMapping();
}
