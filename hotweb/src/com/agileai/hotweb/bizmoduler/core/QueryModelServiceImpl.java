package com.agileai.hotweb.bizmoduler.core;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;

public class QueryModelServiceImpl extends BaseService implements QueryModelService {
	public DataRow findDetail(DataParam param) {
		String statementId = sqlNameSpace+"."+"findDetail";
		DataRow result = this.daoHelper.getRecord(statementId, param);
		return result;
	}

	public List<DataRow> findRecords(DataParam param) {
		String statementId = sqlNameSpace+"."+"queryRecords";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}
}
