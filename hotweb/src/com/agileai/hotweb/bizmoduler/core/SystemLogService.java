package com.agileai.hotweb.bizmoduler.core;

public interface SystemLogService extends BaseInterface{
	void insertLogRecord(String ipAddress,String userId,String userName
			,String funcName,String actionType);
}
