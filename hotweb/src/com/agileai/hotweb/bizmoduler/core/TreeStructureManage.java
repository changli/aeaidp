package com.agileai.hotweb.bizmoduler.core;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;

public interface TreeStructureManage extends BaseInterface{
	List<DataRow> findTreeRecords(DataParam param);
	List<DataRow> queryChildTreeRecords(String currentId);
	int retrieveNewMaxSort(String parentId);
	DataRow queryTreeRecord(DataParam param);
	void createTreeRecord(DataParam param);
	void createTreeRecord(DataParam param, String pKeyValue);
	void updateTreeRecord(DataParam param);
	void deleteTreeRecord(String currentId);
	boolean isLastTreeChild(String currentId);
	boolean isFirstTreeChild(String currentId);
	void changeTreeSort(String currentId,boolean isUp);
}
