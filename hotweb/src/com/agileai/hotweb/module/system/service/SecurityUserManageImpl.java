package com.agileai.hotweb.module.system.service;

import java.util.ArrayList;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeAndContentManageImpl;
import com.agileai.util.CryptionUtil;
import com.agileai.util.StringUtil;

public class SecurityUserManageImpl 
		extends TreeAndContentManageImpl
		implements 	SecurityUserManage {
	public SecurityUserManageImpl() {
		super();
		this.columnIdField = "GRP_ID";
		this.columnParentIdField = "GRP_PID";
		this.columnSortField = "GRP_SORT";
	}

	@Override
	public DataRow getCopyContentRecord(String grpId,String userId) {
		DataParam param = new DataParam();
		param.put("GRP_ID", grpId);
		param.put("USER_ID", userId);
		DataRow result=null;
		String statementId = sqlNameSpace+"."+"queryCopyTreeContentRelation";		
		List<DataRow> tempRecords = this.daoHelper.queryRecords(statementId, param);
		if (tempRecords.size()>=1){
			result = tempRecords.get(0);
		}
		return result;		
	}

	@Override
	public DataRow getMoveContentRecord(String grpId,String userId) {
		DataParam param = new DataParam();
		param.put("GRP_ID", grpId);
		param.put("USER_ID", userId);
		DataRow result=null;
		String statementId = sqlNameSpace+"."+"queryMoveTreeContentRelation";		
		List<DataRow> tempRecords = this.daoHelper.queryRecords(statementId, param);
		if (tempRecords.size()>=1){
			result=tempRecords.get(0);
		}
		return result;		
	}

	@Override
	public void deleteSecurityUserRecord(String userId) {
		DataParam param = new DataParam();
		param.put("USER_ID", userId);
		String statementId = sqlNameSpace+"."+"deleteSecurityUserRecord";
		this.daoHelper.deleteRecords(statementId,param);
	}

	@Override
	public void deleteSecurityUserRelation(String grpId,String userId) {
		DataParam param = new DataParam();
		param.put("GRP_ID", grpId);
		param.put("USER_ID", userId);
		String statementId = sqlNameSpace+"."+"deleteSecurityUserRelation";
		this.daoHelper.deleteRecords(statementId,param);
	}

	@Override
	public void createtContentRecord(String tabId,DataParam param) {
		String statementId = sqlNameSpace+"."+"insert"+StringUtil.upperFirst(tabId)+"Record";
		String curTableName = tabIdAndTableNameMapping.get(tabId);
		processDataType(param, curTableName);
		this.daoHelper.insertRecord(statementId, param);
		String tableMode = tabIdAndTableModeMapping.get(tabId);
		if (TableMode.Many2ManyAndRel.equals(tableMode)){
			statementId = sqlNameSpace+"."+"insert"+StringUtil.upperFirst(tabId)+"Relation";
			this.daoHelper.insertRecord(statementId, param);			
		}
	}

	@Override
	public List<DataRow> findRoleTreeRecords(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"findRoleTreeRecords";
		return this.daoHelper.queryRecords(statementId, param);
	}

	protected String SendBackPassword(String sendPassword,String secretKey){
		String Password = "";
		String SendSK = "";
		StringBuffer SBSK = new StringBuffer(secretKey); 
		if(SBSK.length()>=8){
		SendSK = secretKey.substring(0, 8);
	}
	else{
		while(SBSK.length()<8){
			SBSK=SBSK.append("x");
		}
		SendSK = SBSK.toString();
	}
	Password=CryptionUtil.decryption(sendPassword, SendSK);
	return Password;
	}

	protected String toSendPassword(String password,String secretKey){
		String SendPW = "";
		String SendSK = "";
		StringBuffer SBSK = new StringBuffer(secretKey); 
		if(SBSK.length()>=8){
		SendSK = secretKey.substring(0, 8);
		}
		else{
			while(SBSK.length()<8){
				SBSK=SBSK.append("x");
			}
			SendSK = SBSK.toString();
		}
		SendPW=CryptionUtil.encryption(password, SendSK);
		return SendPW;
	}

	@Override
	public List<DataRow> retrieveUserList(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"retrieveUserList";
		return this.daoHelper.queryRecords(statementId, param);
	}

	@Override
	public List<DataRow> findSecurityUserRelation(String grpId,String userId) {
		List<DataRow> result = new ArrayList<DataRow>();
		DataParam param = new DataParam();
		param.put("GRP_ID", grpId);
		param.put("USER_ID", userId);
		String statementId = this.sqlNameSpace+"."+"querySecurityUserRelation";
		result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}

	
	public String buildCacheKey(String dataId){
		return "SecurityUser_User_"+dataId;
	}

	@Override
	public List<DataRow> findUserActualRoleRecords(String rgId) {
		List<DataRow> result = new ArrayList<DataRow>();
		DataParam param = new DataParam();
		param.put("RG_ID", rgId);
		String statementId = this.sqlNameSpace+"."+"findUserActualRoleRecords";
		result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}

	@Override
	public List<DataRow> findPickFillRecords(String rgId) {
		List<DataRow> result = new ArrayList<DataRow>();
		DataParam param = new DataParam();
		param.put("RG_ID", rgId);
		String statementId = this.sqlNameSpace+"."+"queryPickFillRecords";
		result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}

	@Override
	public void insertUserActualRoleRecords(DataParam param) {
		String statementId = sqlNameSpace+"."+"insertUserActualRoleRecords";
		this.daoHelper.insertRecord(statementId, param);	
	}

	@Override
	public void deletTureContentRecord(String urgId) {
		String statementId = sqlNameSpace+"."+"deleteTrueRelation";
		DataParam param=new DataParam();
		param.put("URG_ID",urgId);
		this.daoHelper.deleteRecords(statementId, param);
	}

	@Override
	public DataRow getUserActualRoleRelation(String rgId, String userId) {
		String statementId = sqlNameSpace+"."+"getUserActualRoleRelation";
		DataParam param=new DataParam();
		param.put("USER_ID",userId);
		param.put("RG_ID",rgId);
		DataRow result = this.daoHelper.getRecord(statementId, param);
		return result;
	}
}
