package com.agileai.hotweb.controller.core;

import java.io.File;

import com.agileai.common.AppConfig;
import com.agileai.domain.DataParam;
import com.agileai.util.DateUtil;
import com.agileai.util.StringUtil;

public class FileUploadHandler extends BaseHandler{
	protected DataParam resourceParam = new DataParam();
	protected String resourceRelativePath = null;
	
	public FileUploadHandler(){
		super();
	}
	
	
	protected String getParentAbsolutePath(){
		File contextFile = new File(System.getProperty("catalina.base")+"/webapps");
		AppConfig appConfig = this.getAppConfig();
		String parentAbsolutePath = appConfig.getConfig("GlobalConfig", "FileStorePath");
		if (StringUtil.isNullOrEmpty(parentAbsolutePath)){
			parentAbsolutePath = contextFile.getAbsolutePath();			
		}
		return parentAbsolutePath;
	}
	
	protected File buildResourseSavePath(String columnId){
		File result = null;
		String parentAbsolutePath = this.getParentAbsolutePath();
		
		String resourseFilePath = parentAbsolutePath+File.separator+"HotServer"
				+File.separator+"reponsitory"+File.separator+"resourse"+File.separator +columnId
				+File.separator+DateUtil.getYear()+File.separator+DateUtil.getMonth()+File.separator+DateUtil.getDay();
		
		resourceRelativePath = "/HotServer"+"/reponsitory"+"/resourse/"+columnId
				+"/"+DateUtil.getYear()+"/"+DateUtil.getMonth()+"/"+DateUtil.getDay();
		result = new File(resourseFilePath);
		return result;
	}
	
	protected void checkSuffix(String suffix,String resTypeExts) throws Exception{
		String[] resTypeExtArray = resTypeExts.split(";");
		try {
			if (StringUtil.isNullOrEmpty(resTypeExts) || resTypeExts.indexOf("*.*") > -1){
				throw new Exception("resource type is unsafe,it must be specified some types");
			}
			boolean isMatched = false;
			for (int i= 0;i < resTypeExtArray.length;i++){
				String resTypeExt = resTypeExtArray[i];
				if (resTypeExt.toLowerCase().trim().endsWith(suffix.toLowerCase())){
					isMatched = true;
					break;
				}
			}
			if (!isMatched){
				throw new Exception("resource type is not valid ,please check the uploading file suffix !");
			}
		} catch (Exception e) {
			throw e;
		}
	}
}
