package com.agileai.hotweb.controller.core;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.MasterSubService;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.MapUtil;

public class MasterSubEditPboxHandler extends BaseHandler{
	protected String subTableId = null;

	public ViewRenderer prepareDisplay(DataParam param) {
		String operaType = param.get(OperaType.KEY);
		if (isReqRecordOperaType(operaType)){
			DataRow record = getService().getSubRecord(subTableId, param);
			this.setAttributes(record);			
		}
		this.setOperaType(operaType);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
	public ViewRenderer doSaveAction(DataParam param){
		String responseText = FAIL;
		String operateType = param.get(OperaType.KEY);
		if (OperaType.CREATE.equals(operateType)){
			getService().createSubRecord(subTableId, param);
			responseText = SUCCESS;
		}
		else if (OperaType.UPDATE.equals(operateType)){
			getService().updateSubRecord(subTableId, param);
			responseText = SUCCESS;
		}
		return new AjaxRenderer(responseText);
	}
	public ViewRenderer doCheckUniqueAction(DataParam param){
		String responseText = "";
		DataRow record = getService().getSubRecord(subTableId, param);
		if (!MapUtil.isNullOrEmpty(record)){
			responseText = defDuplicateMsg;
		}
		return new AjaxRenderer(responseText);
	}
	protected MasterSubService getService() {
		return (MasterSubService)this.lookupService(this.getServiceId());
	}
}
