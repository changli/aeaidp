package com.agileai.hotweb.controller.core;

import java.util.ArrayList;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.MasterSubService;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.MapUtil;

public abstract class MasterSubEditMainHandler extends BaseHandler{
	@SuppressWarnings("rawtypes")
	protected Class listHandlerClass = null; 
	protected String defaultTabId = "param";
	protected String baseTablePK = "";
	
	protected String moveUpErrorMsg = "该节点是第一个节点，不能上移！";
	protected String moveDownErrorMsg = "该节点是最后一个节点，不能下移！";

	public ViewRenderer prepareDisplay(DataParam param) {
		String operaType = param.get(OperaType.KEY);
		if (isReqRecordOperaType(operaType)){
			DataRow record = getService().getMasterRecord(param);
			this.setAttributes(record);			
		}
		String currentSubTableId = param.get("currentSubTableId",defaultTabId);
		if (!currentSubTableId.equals(MasterSubService.BASE_TABLE_ID)){
			String subRecordsKey = currentSubTableId + "Records";
			if (!this.getAttributesContainer().containsKey(subRecordsKey)){
				List<DataRow> subRecords = getService().findSubRecords(currentSubTableId, param);
				this.setAttribute(currentSubTableId+"Records", subRecords);
			}
		}
		this.setAttribute("currentSubTableId", currentSubTableId);
		this.setAttribute("currentSubTableIndex", getTabIndex(currentSubTableId));
		String operateType = param.get(OperaType.KEY);
		this.setOperaType(operateType);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
	
	public ViewRenderer doSaveMasterRecordAction(DataParam param){
		String operateType = param.get(OperaType.KEY);
		String responseText = "fail";
		if (OperaType.CREATE.equals(operateType)){
			getService().createMasterRecord(param);
			responseText = param.get(baseTablePK);
		}
		else if(OperaType.UPDATE.equals(operateType)){
			getService().updateMasterRecord(param);
			saveSubRecords(param);
			responseText = param.get(baseTablePK);
		}
		return new AjaxRenderer(responseText);
	}
	
	public ViewRenderer doMoveUpAction(DataParam param){
		String currentSubTableId = param.get("currentSubTableId");
		boolean isFirst = getService().isFirstChild(currentSubTableId,param);
		if (isFirst){
			setErrorMsg(this.moveUpErrorMsg);
		}else{
			getService().changeCurrentSort(param, true);
		}
		return prepareDisplay(param);
	}
	
	public ViewRenderer doMoveDownAction(DataParam param){
		String currentSubTableId = param.get("currentSubTableId");
		boolean isLast = getService().isLastChild(currentSubTableId,param);
		if (isLast){
			setErrorMsg(this.moveDownErrorMsg);
		}else{
			getService().changeCurrentSort(param, false);
		}		
		return prepareDisplay(param);
	}	
	
	public ViewRenderer doChangeSubTableAction(DataParam param){
		return prepareDisplay(param);
	}	
	
	protected String getTabIndex(String currentSubTableId) {
		String result = "0";
		String[] subTableIds = getService().getTableIds();
		for (int i = 0; i < subTableIds.length; i++) {
			String subId = subTableIds[i].toString();
			if (currentSubTableId.equals(subId)) {
				result = String.valueOf(i);
				break;
			}
		}
		return result;
	}
	
	protected void saveSubRecords(DataParam param){
		param.buildParam("state_");
		List<DataParam> updateList = param.getUpdateParam();
		List<DataParam> insertList = param.getInsertParam();
		if (updateList.size() > 0 || insertList.size() > 0){
			this.getService().saveSubRecords(param, insertList, updateList);			
		}
	}
	
	public ViewRenderer doSaveEntryRecordsAction(DataParam param){
		saveSubRecords(param);
		return prepareDisplay(param);
	}
	
	public ViewRenderer doDeleteSubRecordAction(DataParam param){
		String currentSubTableId = param.get("currentSubTableId");
		getService().deleteSubRecord(currentSubTableId, param);
		return prepareDisplay(param); 
	}
	
	public ViewRenderer doCheckUniqueAction(DataParam param){
		String responseText = "";
		DataRow record = getService().getMasterRecord(param);
		if (!MapUtil.isNullOrEmpty(record)){
			responseText = defDuplicateMsg;
		}
		return new AjaxRenderer(responseText);
	}
	
	public ViewRenderer doAddEntryRecordAction(DataParam param){
		String currentSubTableId = param.get("currentSubTableId");
		int currentRecordSize = param.getInt("currentRecordSize");
		List<DataRow> subRecords = new ArrayList<DataRow>();
		String[] entryEditFields = this.getEntryEditFields(currentSubTableId);
		for (int i=0;i < currentRecordSize;i++){
			DataRow row = new DataRow();
			for (int j=0; j < entryEditFields.length;j++){
				String field = entryEditFields[j];
				row.put(field,param.get(field+"_"+i));
			}
			row.put("_state",param.get("state_"+i));
			subRecords.add(row);
		}
		String foreignKey = this.getEntryEditForeignKey(currentSubTableId);
		subRecords.add(new DataRow("_state","insert",foreignKey,param.get(baseTablePK)));
		String subRecordsKey = currentSubTableId + "Records";
		this.setAttribute(subRecordsKey, subRecords);
		return prepareDisplay(param); 
	}
	
	public ViewRenderer doDeleteEntryRecordAction(DataParam param){
		String currentSubTableId = param.get("currentSubTableId");
		int currentRecordIndex = param.getInt("currentRecordIndex");
		String state = param.get("state_"+currentRecordIndex);
		
		
		int currentRecordSize = param.getInt("currentRecordSize");
		List<DataRow> subRecords = new ArrayList<DataRow>();
		String[] entryEditFields = this.getEntryEditFields(currentSubTableId);
		for (int i=0;i < currentRecordSize;i++){
			if (i == currentRecordIndex)continue;
			DataRow row = new DataRow();
			for (int j=0; j < entryEditFields.length;j++){
				String field = entryEditFields[j];
				row.put(field,param.get(field+"_"+i));
			}
			row.put("_state",param.get("state_"+i));
			subRecords.add(row);
		}
		String subRecordsKey = currentSubTableId + "Records";
		this.setAttribute(subRecordsKey, subRecords);
		
		if (!state.equals("insert")){
			DataParam deleteParam = new DataParam();
			String pKField = getEntryEditTablePK(currentSubTableId);
			deleteParam.put(pKField,param.get(pKField+"_"+currentRecordIndex));
			getService().deleteSubRecord(currentSubTableId, deleteParam);
		}
		return prepareDisplay(param);		
	}	
	abstract protected String[] getEntryEditFields(String currentSubTableId);
	abstract protected String getEntryEditTablePK(String currentSubTableId);
	abstract protected String getEntryEditForeignKey(String currentSubTableId);
	
	public ViewRenderer doBackAction(DataParam param){
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}
	protected MasterSubService getService() {
		return (MasterSubService)this.lookupService(this.getServiceId());
	}
}