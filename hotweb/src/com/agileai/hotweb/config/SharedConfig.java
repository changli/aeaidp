package com.agileai.hotweb.config;

public interface SharedConfig {
	
	public static class Keys {
		public static final String casServerLogoutUrl = "casServerLogoutUrl";
		public static final String casServerLoginUrl = "casServerLoginUrl";
		public static final String serverName = "serverName";
		public static final String casServerUrlPrefix = "casServerUrlPrefix";
	}
	
	public boolean isEnable();
	public String getConfig(String key);	
	public String getConfig(String key,String defaultValue);
	
}