package com.agileai.hotweb.config;

public class OauthConfig {
	private String oauthSource = null;
	private String appId = null;
	private String appSecret = null;
	private String oauthServerURL = null;
	private String callbackURL = null;
	private String state = null;
	
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getAppSecret() {
		return appSecret;
	}
	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}
	public String getOauthServerURL() {
		return oauthServerURL;
	}
	public void setOauthServerURL(String oauthServerURL) {
		this.oauthServerURL = oauthServerURL;
	}
	public String getCallbackURL() {
		return callbackURL;
	}
	public void setCallbackURL(String callbackURL) {
		this.callbackURL = callbackURL;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getOauthSource() {
		return oauthSource;
	}
	public void setOauthSource(String oauthSource) {
		this.oauthSource = oauthSource;
	}
}