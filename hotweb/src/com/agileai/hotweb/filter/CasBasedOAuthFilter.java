package com.agileai.hotweb.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jettison.json.JSONObject;

import com.agileai.common.AppConfig;
import com.agileai.hotweb.common.BeanFactory;
import com.agileai.hotweb.common.OauthHelper;
import com.agileai.hotweb.config.OauthConfig;
import com.agileai.hotweb.oauth.IOauthService;
import com.agileai.util.StringUtil;

public class CasBasedOAuthFilter implements Filter{
	
	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest)servletRequest;
		HttpServletResponse response = (HttpServletResponse)servletResponse;
		
		if (isOauthCallbackRequest(request)) {
			String code = request.getParameter("code");
			HttpSession httpSession = request.getSession();
			OauthConfig oauthConfig = (OauthConfig)httpSession.getAttribute(OauthConfig.class.getName());
			String oauthSource = oauthConfig.getOauthSource();
			AppConfig appConfig = BeanFactory.instance().getAppConfig();
			OauthHelper oauthHelper = new OauthHelper(appConfig);
			String oauthServiceId = oauthHelper.buildOauthServiceId(oauthSource);
			
			IOauthService oauthService = (IOauthService)BeanFactory.instance().getBean(oauthServiceId);
			String profileResponse = oauthService.retrieveProfile(oauthConfig, code);
			if (profileResponse != null) {
				try {
					httpSession.removeAttribute(OauthConfig.class.getName());
					JSONObject jsonObject = new JSONObject(profileResponse);
					if (jsonObject.has("attributes") && jsonObject.has("id")) {
						String userId = jsonObject.getString("id");
						this.initUserProfile(request,response,userId,jsonObject);
						String srcRedirectURL = oauthConfig.getCallbackURL();
						response.sendRedirect(srcRedirectURL);							
					}
					else {
						showError(request, response);
					}
				} catch (Exception e) {
					showError(request, response);
				}
			}
			else {
				showError(request, response);
			}
		}
		else {
			filterChain.doFilter(servletRequest, servletResponse);
		}
	}

	protected void initUserProfile(HttpServletRequest request,HttpServletResponse response,String userId,JSONObject jsonObject) {
	
	}
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		
	}
	
	protected void showError(HttpServletRequest request,HttpServletResponse response) {
		try {
			String fileUrl = "/jsp/frame/Error.jsp";
			RequestDispatcher requestDispatcher = request.getServletContext().getRequestDispatcher(fileUrl);
			requestDispatcher.forward(request, response);			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected boolean isOauthCallbackRequest(HttpServletRequest request) {
		boolean result = false;
		String code = request.getParameter("code");
		String state = request.getParameter("state");
		if (StringUtil.isNotNullNotEmpty(code) && StringUtil.isNotNullNotEmpty(state)) {
			HttpSession httpSession = request.getSession();
			OauthConfig oauthConfig = (OauthConfig)httpSession.getAttribute(OauthConfig.class.getName());
			if (oauthConfig != null && state.equals(oauthConfig.getState())) {
				result = true;
			}
		}
		return result;
	}
	
	@Override
	public void destroy() {
	}
}
