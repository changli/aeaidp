package com.agileai.hotweb.cache;

import java.io.Serializable;
import java.util.Set;

public interface CacheService {
	void putValue(String key,Serializable serializable);
	Object getValue(String key);
	void removeValue(String key);
	boolean existsValue(String key);
	boolean isDistributed();
	
	void addSet(String setKey,String member);
	void removeSet(String setKey);
	void removeSet(String setKey,String member);
	Set<String> getSet(String setKey);
	boolean isInSet(String setKey,String member);
	boolean isExistSetValue(String setKey);
}
