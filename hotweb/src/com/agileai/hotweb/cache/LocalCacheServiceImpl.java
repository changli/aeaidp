package com.agileai.hotweb.cache;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class LocalCacheServiceImpl implements CacheService{
	private ConcurrentHashMap<String,Object> cacher = new ConcurrentHashMap<String,Object>();
	private ConcurrentHashMap<String,Set<String>> setCacher = new ConcurrentHashMap<String,Set<String>>();
	
	public LocalCacheServiceImpl(){
	}
	
	@Override
	public void putValue(String key, Serializable serializable) {
		this.cacher.put(key, serializable);
	}

	@Override
	public Object getValue(String key) {
		Object object = this.cacher.get(key);
		return object;
	}

	@Override
	public boolean existsValue(String key) {
		return cacher.containsKey(key);
	}

	@Override
	public void removeValue(String key) {
		this.cacher.remove(key);
	}
	
	@Override
	public boolean isDistributed() {
		return false;
	}

	@Override
	public void addSet(String setKey, String member) {
		Set<String> set = setCacher.get(setKey);
		if (set == null){
			set = new HashSet<String>();
			setCacher.put(setKey, set);
		}
		set.add(member);
	}

	@Override
	public void removeSet(String setKey, String member) {
		Set<String> set = setCacher.get(setKey);
		if (set == null){
			set = new HashSet<String>();
			setCacher.put(setKey, set);
		}
		if (set.contains(member)){
			set.remove(member);
		}
	}

	@Override
	public Set<String> getSet(String setKey) {
		Set<String> set = setCacher.get(setKey);
		if (set == null){
			set = new HashSet<String>();
			setCacher.put(setKey, set);
		}
		return set;
	}

	@Override
	public boolean isInSet(String setKey, String member) {
		Set<String> set = setCacher.get(setKey);
		if (set == null){
			set = new HashSet<String>();
			setCacher.put(setKey, set);
		}
		return set.contains(member);
	}

	@Override
	public void removeSet(String setKey) {
		if (setCacher.containsKey(setKey)) {
			setCacher.remove(setKey);
		}
	}

	@Override
	public boolean isExistSetValue(String setKey) {
		Set<String> set = setCacher.get(setKey);
		if (set == null || set.isEmpty()){
			return false;
		}
		else {
			return true;
		}
	}
}