package com.agileai.hotweb.oauth;

import com.agileai.hotweb.common.HttpClientHelper;
import com.agileai.hotweb.config.OauthConfig;
import com.agileai.util.StringUtil;

public class AidmOauthService extends BaseOauthService implements IOauthService{

	@Override
	public String retrieveProfile(OauthConfig oauthConfig,String code) {
		String result = null;
		String accessTokenApiURL = this.buildAccessTokenApiURL(oauthConfig,code);
		
		HttpClientHelper httpClientHelper = new HttpClientHelper();
		String accessTokenResponse = httpClientHelper.retrieveGetReponseText(accessTokenApiURL);
		if (StringUtil.isNotNullNotEmpty(accessTokenResponse)) {
			int lastIndex = 0;
			if (accessTokenResponse.indexOf("&") > -1) {
				lastIndex = accessTokenResponse.indexOf("&");
			}
			else {
				lastIndex = accessTokenResponse.length();
			}
			String accessToken = accessTokenResponse.substring("access_token=".length(),lastIndex);
			String profileApiURL = this.buildProfileApiURL(oauthConfig,accessToken);
			String profileResponse = httpClientHelper.retrieveGetReponseText(profileApiURL);
			if (profileResponse != null) {
				result = profileResponse;
			}
		}
		return result;
	}
	
	public String getAuthorizeURL(OauthConfig oauthConfig) {
		String result = "";

		String callbackURL = oauthConfig.getCallbackURL();
		String state = oauthConfig.getState();
		result = oauthConfig.getOauthServerURL()+"/authorize";
		String appId = oauthConfig.getAppId();

		result = this.addParameter(result, "client_id", appId);
		result = this.addParameter(result, "redirect_uri", callbackURL);
		result = this.addParameter(result, "response_type", "code");
		result = this.addParameter(result, "state", state);
		
		return result;
	}
}
