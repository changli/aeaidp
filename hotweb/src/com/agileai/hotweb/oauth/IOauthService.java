package com.agileai.hotweb.oauth;

import com.agileai.hotweb.config.OauthConfig;

public interface IOauthService {
	public String retrieveProfile(OauthConfig oauthConfig,String code);
	public String getAuthorizeURL(OauthConfig oauthConfig);
}
