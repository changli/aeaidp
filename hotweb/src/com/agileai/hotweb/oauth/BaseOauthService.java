package com.agileai.hotweb.oauth;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.agileai.hotweb.config.OauthConfig;

public class BaseOauthService {
	
	protected String addParameter(final String url, final String name, final String value) {
		final StringBuilder sb = new StringBuilder();
		sb.append(url);
		if (url.indexOf("?") >= 0) {
			sb.append("&");
		} else {
			sb.append("?");
		}
		sb.append(name);
		sb.append("=");
		if (value != null) {
			try {
				sb.append(URLEncoder.encode(value, "UTF-8"));
			} catch (final UnsupportedEncodingException e) {
				throw new RuntimeException(e);
			}
		}
		return sb.toString();
	}

	protected String buildAccessTokenApiURL(OauthConfig oauthConfig, String code) {
		String result = "";
		result = oauthConfig.getOauthServerURL() + "/accessToken";

		String appId = oauthConfig.getAppId();
		String appSecret = oauthConfig.getAppSecret();
		String callbackURL = oauthConfig.getCallbackURL();
		result = this.addParameter(result, "client_id", appId);
		result = this.addParameter(result, "client_secret", appSecret);
		result = this.addParameter(result, "grant_type", "authorization_code");
		result = this.addParameter(result, "redirect_uri", callbackURL);
		result = this.addParameter(result, "code", code);

		return result;
	}

	protected String buildProfileApiURL(OauthConfig oauthConfig, String accessToken) {
		String result = oauthConfig.getOauthServerURL() + "/profile";
		result = this.addParameter(result, "access_token", accessToken);
		return result;
	}
}
