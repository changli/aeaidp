package com.agileai.hotweb.common;

import java.util.ArrayList;
import java.util.List;

import com.agileai.hotweb.bizmoduler.frame.FunctionManage;
import com.agileai.hotweb.domain.TreeBuilder;
import com.agileai.hotweb.domain.TreeModel;
import com.agileai.hotweb.domain.core.Resource;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.domain.system.FuncMenu;
import com.agileai.hotweb.filter.HotwebUserCacher;

public class HotwebAuthHelper {
	private static final String CurrentFuncList = "funcsList";
	public static final String AutherHandlerCodes = HotwebUserCacher.AutherHandlerCodes;

	private User user = null;
	
	public HotwebAuthHelper(User user){
		this.user = user;
	}
	public FuncMenu getFunction(String functionId){
		BeanFactory beanFactory = BeanFactory.instance();
		FunctionManage functionTreeManage = (FunctionManage)beanFactory.getBean("functionTreeManageService");
		return functionTreeManage.getFunction(functionId);
	}
	public String getTreeSyntax(){
    	String result = null;
    	StringBuffer treeSyntax = new StringBuffer();
    	try {
    		List<FuncMenu> funcRecords = this.getAuthedFuncList();
    		treeSyntax.append("<script type='text/javascript'>");
    		treeSyntax.append("d = new dTree('d');");
    		TreeBuilder treeBuilder = new TreeBuilder(funcRecords,"funcId","funcName","funcPid");
    		treeBuilder.setTypeKey("funcType");
    		treeBuilder.setPojoList(true);
            TreeModel treeModel = treeBuilder.buildTreeModel();
            String rootFuncId = treeModel.getId();
            String rootFuncName = treeModel.getName();
            treeSyntax.append("d.add('"+rootFuncId+"','-1','"+rootFuncName+"',null);");
            treeSyntax.append("\r\n");
            buildTreeSyntax(treeSyntax,treeModel);
            treeSyntax.append("\r\n");
            treeSyntax.append("document.write(d);");
            treeSyntax.append("\r\n");
    		treeSyntax.append("</script>");
    		result = treeSyntax.toString();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
    }
	
	private void buildTreeSyntax(StringBuffer treeSyntax,TreeModel treeModel){
		List<TreeModel> children = treeModel.getChildren();
		String parentId = treeModel.getId();
        for (int i=0;i < children.size();i++){
        	TreeModel child = children.get(i);
            String curFuncId = child.getId();
            String curFuncName = child.getName();
            String curNodeType = child.getType();
            if ("funcmenu".equals(curNodeType)){
            	treeSyntax.append("d.add('"+curFuncId+"','"+parentId+"','"+curFuncName+"',\"javascript:void(0)\",null,null,d.icon.folder,d.icon.folder.folderOpen,null);");
            	treeSyntax.append("\r\n");
            }else{
            	treeSyntax.append("d.add('"+curFuncId+"','"+parentId+"','"+curFuncName+"',\"javascript:showFunction(\\\'"+curFuncId+"\\\')\");");
            	treeSyntax.append("\r\n");
            }
            this.buildTreeSyntax(treeSyntax,child);
        }
    }
	
	@SuppressWarnings("unchecked")
	private List<FuncMenu> getAuthedFuncList(){
		List<FuncMenu> result = null;
		BeanFactory beanFactory = BeanFactory.instance();
		FunctionManage functionTreeManage = (FunctionManage)beanFactory.getBean("functionTreeManageService");
		if (user.isAdmin()){
			result = functionTreeManage.getFuncMenuList();
		}else{
			if (!user.getTempProperties().containsKey(CurrentFuncList)){
				List<FuncMenu> funcs = new ArrayList<FuncMenu>();
				List<FuncMenu> funcList = functionTreeManage.getFuncMenuList();
				for (int i=0;i < funcList.size();i++){
					FuncMenu function = funcList.get(i);
					String functionId = function.getFuncId();
					if (user.containResouce(Resource.Type.Menu, functionId)){
						funcs.add(function);						
					}
				}
				user.getTempProperties().put(CurrentFuncList, funcs);
			}
			result = (List<FuncMenu>)user.getTempProperties().get(CurrentFuncList);			
		}
		return result;
	}
}