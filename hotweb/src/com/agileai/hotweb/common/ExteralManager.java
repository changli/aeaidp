package com.agileai.hotweb.common;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.spi.Provider;

import org.apache.cxf.binding.BindingFactoryManager;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSBindingFactory;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.model.wadl.WadlGenerator;
import org.apache.cxf.jaxws.EndpointImpl;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;

import com.agileai.common.KeyValuePair;
import com.agileai.hotweb.ws.AuthConfig;
import com.agileai.hotweb.ws.BaseRestService;
import com.agileai.hotweb.ws.BaseWebService;
import com.agileai.hotweb.ws.RestServerIntercetper;
import com.agileai.hotweb.ws.ServiceModel;
import com.agileai.hotweb.ws.SoapServerIntercetper;
import com.agileai.util.IOUtil;
import com.agileai.util.StringUtil;
import com.agileai.util.XmlUtil;

public class ExteralManager {
	public static HashMap<ClassLoader,ExteralManager> ServicerCache = new HashMap<ClassLoader,ExteralManager>();
	private HashMap<String,Server> restServiceServerMap = new HashMap<String,Server>();
	private HashMap<String,EndpointImpl> soapServiceFactoryMap = new HashMap<String,EndpointImpl>();
	
	private String basePath = null;
	private ModuleManager moduleManager = null;
	private HashMap<String,List<String>> moduleServiceMap = new HashMap<String,List<String>>();
	private ServletContext servletContext = null;
	
	private ExteralManager(ClassLoader appClassLoader){
		this.basePath = System.getProperty("catalina.base")+"/";
	}
	
	public static synchronized ExteralManager getOnly(ServletContext servletContext){
		ClassLoader appClassLoader = servletContext.getClassLoader();
		ExteralManager result = null;
		if (!ServicerCache.containsKey(appClassLoader)){
			ExteralManager moduleManager = new ExteralManager(appClassLoader);
			moduleManager.moduleManager = ModuleManager.getOnly(appClassLoader);
			moduleManager.servletContext = servletContext;
			ServicerCache.put(appClassLoader, moduleManager);
		}
		result = ServicerCache.get(appClassLoader);
		return result;
	}
	
	public List<ServiceModel> getRestServiceModels(String appName){
		List<ServiceModel> serviceModels = new ArrayList<ServiceModel>();
		List<String> allModuleList = moduleManager.getAllModuleNameList();
		for (int i=0;i < allModuleList.size();i++){
			String moduleName = allModuleList.get(i);
			StringBuffer configPath = new StringBuffer();
			configPath.append(basePath).append("webapps/").append(appName).append("/WEB-INF/modules/"+moduleName+"/");
			configPath.append("ExteralModule.xml");
			this.parseServiceModels(serviceModels,configPath.toString(),"Rest",moduleName);
		}
		return serviceModels;
	}
	
	public List<ServiceModel> getSoapServiceModels(String appName){
		List<ServiceModel> serviceModels = new ArrayList<ServiceModel>();
		List<String> allModuleList = moduleManager.getAllModuleNameList();
		for (int i=0;i < allModuleList.size();i++){
			String moduleName = allModuleList.get(i);
			StringBuffer configPath = new StringBuffer();
			configPath.append(basePath).append("webapps/").append(appName).append("/WEB-INF/modules/"+moduleName+"/");
			configPath.append("ExteralModule.xml");
			
			this.parseServiceModels(serviceModels,configPath.toString(),"Soap",moduleName);
		}
		return serviceModels;
	}
	
	public List<ServiceModel> getRestServiceModels(String appName,String moduleName){
		List<ServiceModel> serviceModels = new ArrayList<ServiceModel>();
		StringBuffer configPath = new StringBuffer();
		configPath.append(basePath).append("webapps/").append(appName).append("/WEB-INF/modules/"+moduleName+"/");
		configPath.append("ExteralModule.xml");
		this.parseServiceModels(serviceModels,configPath.toString(),"Rest",moduleName);
		return serviceModels;
	}
	
	public List<ServiceModel> getSoapServiceModels(String appName,String moduleName){
		List<ServiceModel> serviceModels = new ArrayList<ServiceModel>();
		StringBuffer configPath = new StringBuffer();
		configPath.append(basePath).append("webapps/").append(appName).append("/WEB-INF/modules/"+moduleName+"/");
		configPath.append("ExteralModule.xml");
		
		this.parseServiceModels(serviceModels,configPath.toString(),"Soap",moduleName);
		return serviceModels;
	}
	
	
	public HashMap<String, Server> getRestServiceServerMap() {
		return restServiceServerMap;
	}

	public HashMap<String, EndpointImpl> getSoapServiceFactoryMap() {
		return soapServiceFactoryMap;
	}

	@SuppressWarnings("unchecked")
	private void parseServiceModels(List<ServiceModel> serviceModels,String configPath,String serviceType,String moduleName){
		try {
			File configFile = new File (configPath);
			if (configFile.exists()){
				FileInputStream serviceConfigFile = new FileInputStream(configFile);
				Document document = XmlUtil.readDocument(serviceConfigFile);
				String servicePath = "//beans/service[@type='"+serviceType+"']";
				List<Node> nodes = document.selectNodes(servicePath);
				if (nodes != null){
					for (int i=0;i < nodes.size();i++){
						Element serviceElement = (Element)nodes.get(i);
						String type = serviceElement.attributeValue("type");
						String implClass = serviceElement.attributeValue("implClass");
						String id = serviceElement.attributeValue("id");
						String address = serviceElement.attributeValue("address");
						String initParam = serviceElement.attributeValue("initParam");
						
						ServiceModel serviceModel = new ServiceModel();
						serviceModel.setId(id);
						serviceModel.setAddress(address);
						serviceModel.setImplClass(implClass);
						serviceModel.setServiceType(type);
						if (StringUtil.isNotNullNotEmpty(initParam)){
							serviceModel.setInitParam(initParam);
						}
						
						List<String> serviceIdList = this.getServiceIdList(moduleName);
						if (serviceIdList.contains(id)){
							System.err.println("exteral service " + id + " is exists , please check it !");
						}else{
							serviceIdList.add(id);
						}
						
						List<Element> auths = serviceElement.elements("auth");
						if (auths != null){
							for (int j=0; j < auths.size();j++){
								Element auth = auths.get(j);
								AuthConfig authConfig = new AuthConfig();
								String authType = auth.attributeValue("type");
								authConfig.setType(authType);
								List<Element> properties = auth.elements("property");
								if (properties != null){
									for (int x=0;x < properties.size();x++){
										Element property = properties.get(x);
										String name = property.attributeValue("name");
										String value = property.getTextTrim();
										
										KeyValuePair keyValuePair = new KeyValuePair();
										keyValuePair.setKey(name);
										keyValuePair.setValue(value);
										
										authConfig.getProperties().put(name, keyValuePair);
									}
								}
								serviceModel.getAuthConfigs().add(authConfig);
							}
						}
						serviceModels.add(serviceModel);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private List<String> getServiceIdList(String moduleName){
		List<String> result = null;
		if (!this.moduleServiceMap.containsKey(moduleName)){
			this.moduleServiceMap.put(moduleName, new ArrayList<String>());
		}
		result = this.moduleServiceMap.get(moduleName);
		return result;
	}
	
	
	public void registryRestService(ServiceModel serviceModel){
		try {
			ClassLoader appClassLoader = Thread.currentThread().getContextClassLoader();
			
			String serviceId = serviceModel.getId();
			String serviceClass = serviceModel.getImplClass();
			String address = serviceModel.getAddress();
			
			JAXRSServerFactoryBean serverFactory = new JAXRSServerFactoryBean();
			ClassLoaderFactory classLoaderFactory = ClassLoaderFactory.instance(appClassLoader);
			
			String moduleName = moduleManager.getExteralModule(serviceId);
			if (StringUtil.isNotNullNotEmpty(moduleName)){
				ClassLoader classLoader = classLoaderFactory.createModuleClassLoader(moduleName);
				
				BaseRestService serviceBean = (BaseRestService)classLoader.loadClass(serviceClass).newInstance();
				serviceBean.setServletContext(servletContext);
				serviceBean.setServiceModel(serviceModel);
				
				serverFactory.setServiceBean(serviceBean);
				serverFactory.setAddress(address);
				
				if (serviceModel.getAuthConfigs().size() > 0){
					RestServerIntercetper serverIntercetper = new RestServerIntercetper();
					serverIntercetper.setServiceModel(serviceModel);
					serverFactory.getInInterceptors().add(serverIntercetper);				
				}
				
				List<Object> providers = new ArrayList<Object>();
				
				WadlGenerator wadlGenerator = new WadlGenerator();
				wadlGenerator.setLinkAnyMediaTypeToXmlSchema(true);
				wadlGenerator.setUseJaxbContextForQnames(true);
				providers.add(wadlGenerator);

				JacksonJsonProvider jacksonJsonProvider = new JacksonJsonProvider();
				ObjectMapper objectMapper = new ObjectMapper();
				jacksonJsonProvider.setMapper(objectMapper);
				providers.add(jacksonJsonProvider);
				
				serverFactory.setProviders(providers);
				
				BindingFactoryManager manager = serverFactory.getBus().getExtension(BindingFactoryManager.class);
				JAXRSBindingFactory factory = new JAXRSBindingFactory();
				factory.setBus(serverFactory.getBus());
				manager.registerBindingFactory(JAXRSBindingFactory.JAXRS_BINDING_ID, factory);
				Server server = serverFactory.create();
				
				this.getRestServiceServerMap().put(serviceModel.getId(), server);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void registrySoapService(ServiceModel serviceModel){
		try {
			ClassLoader appClassLoader = Thread.currentThread().getContextClassLoader();
			
			String serviceId = serviceModel.getId();
			String implClass = serviceModel.getImplClass();
			String address = serviceModel.getAddress();
			
			ClassLoaderFactory classLoaderFactory = ClassLoaderFactory.instance(appClassLoader);
			Object implementor;
			
			String moduleName = moduleManager.getExteralModule(serviceId);
			if (StringUtil.isNotNullNotEmpty(moduleName)){
				ClassLoader classLoader = classLoaderFactory.createModuleClassLoader(moduleName);
				implementor = (Object)classLoader.loadClass(implClass).newInstance();

				BaseWebService baseWebService = (BaseWebService)implementor;
				baseWebService.setServiceModel(serviceModel);
				
				Provider provider = Provider.provider();
				EndpointImpl endPoint = (EndpointImpl)provider.createEndpoint(null, implementor);
				
				if (serviceModel.getAuthConfigs().size() > 0){
					SoapServerIntercetper serverIntercetper = new SoapServerIntercetper();
					serverIntercetper.setServiceModel(serviceModel);
					endPoint.getInInterceptors().add(serverIntercetper);				
				}
				
		        endPoint.publish(address);
		        
		        this.getSoapServiceFactoryMap().put(serviceModel.getId(), endPoint);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	
	protected boolean checkUserAuth(HttpServletRequest request) throws JSONException{
		boolean result = false;
		String inputString = this.getInputString(request,"UTF-8");
		if (StringUtil.isNotNullNotEmpty(inputString)){
			JSONObject requestJson = new JSONObject(inputString);
			String appName = requestJson.getString("appName");
			String securityKey = requestJson.getString("securityKey");
			String memSecurityKey = MemSecrityKeys.getSecrityKey(appName);
			if (securityKey.equals(memSecurityKey)){
				result = true;
			}
		}
		return result;
	}
	
	public String getInputString(HttpServletRequest request,String charset){
		String inputString = null;
		try {
			InputStream inputStream =  request.getInputStream();
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			IOUtil.copyAndCloseInput(inputStream, byteArrayOutputStream);
			inputString = byteArrayOutputStream.toString(charset);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return inputString;
	}	
	
	private void destroySingleService(String serviceId){
		EndpointImpl endPoint = this.getSoapServiceFactoryMap().get(serviceId);
		if (endPoint != null){
			endPoint.stop();
			this.getSoapServiceFactoryMap().remove(serviceId);
		}
		Server server = this.getRestServiceServerMap().get(serviceId);
		if (server != null){
			server.stop();
			server.destroy();
			
			this.getRestServiceServerMap().remove(serviceId);
		}
	}
	
	public void reload(String appName,String moduleName){
		destroyModuleService(moduleName);
		load(appName, moduleName);
	}
	
	public void destroyServices(){
		List<String> modules = moduleManager.getAllModuleNameList();
		for (int i=0;i < modules.size();i++){
			String moduleName = modules.get(i);
			destroyModuleService(moduleName);
		}
	}
	
	public void destroyModuleService(String moduleName){
		if (moduleServiceMap.containsKey(moduleName)){
			List<String> serviceIdList = this.moduleServiceMap.get(moduleName);
			for (int i=0;i < serviceIdList.size();i++){
				String serviceId = serviceIdList.get(i);
				destroySingleService(serviceId);
			}
			this.getServiceIdList(moduleName).clear();
			this.moduleServiceMap.remove(moduleName);			
		}
	}
	
	public boolean load(String appName,String moduleName){
		boolean result = false;
		try {
	        List<ServiceModel> soapModels = this.getSoapServiceModels(appName,moduleName);
	        for (int i=0;i < soapModels.size();i++){
	        	ServiceModel serviceModel = soapModels.get(i);
	        	registrySoapService(serviceModel);
	        }
	        List<ServiceModel> restModels = this.getRestServiceModels(appName,moduleName);
	        for (int i=0;i < restModels.size();i++){
	        	ServiceModel serviceModel = restModels.get(i);
	        	registryRestService(serviceModel);
	        }
	        result = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}