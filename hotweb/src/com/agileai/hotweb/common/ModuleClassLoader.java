package com.agileai.hotweb.common;

import java.net.URL;
import java.net.URLClassLoader;

public class ModuleClassLoader extends URLClassLoader{
	public ModuleClassLoader(URL[] urls) {
		super(urls);
	}
	public ModuleClassLoader(URL[] urls, ClassLoader parent){
		super(urls,parent);
	}
}
