package com.agileai.hotweb.common;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.orm.ibatis.SqlMapClientCallback;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.agileai.domain.DataBag;
import com.agileai.domain.DataBox;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.domain.DataSet;
import com.ibatis.sqlmap.client.SqlMapExecutor;
import com.ibatis.sqlmap.client.event.RowHandler;

public class DaoHelper extends SqlMapClientDaoSupport{
	public DaoHelper(){
		super();
	}
	
	public void batchExecute(SqlMapClientCallback callback) throws DataAccessException{
		this.getSqlMapClientTemplate().execute(callback);
	}
	
	public void batchDelete(final String statementId,final List<DataParam> paramList) throws DataAccessException{
		SqlMapClientCallback callback = new SqlMapClientCallback(){
			public Object doInSqlMapClient(SqlMapExecutor executor) throws SQLException {
				executor.startBatch();
				for (int i=0;i < paramList.size();i++){
					DataParam param = paramList.get(i);
					executor.delete(statementId, param);
				}
				executor.executeBatch();
				return null;
			}
		};
		this.getSqlMapClientTemplate().execute(callback);
	}
	public void batchUpdate(final String statementId,final List<DataParam> paramList) throws DataAccessException{
		SqlMapClientCallback callback = new SqlMapClientCallback(){
			public Object doInSqlMapClient(SqlMapExecutor executor) throws SQLException {
				executor.startBatch();
				for (int i=0;i < paramList.size();i++){
					DataParam dataRow = paramList.get(i);
					executor.update(statementId, dataRow);
				}
				executor.executeBatch();
				return null;
			}
		};
		this.getSqlMapClientTemplate().execute(callback);
	}
	public void batchInsert(final String statementId,final List<DataParam> paramList) throws DataAccessException{
		SqlMapClientCallback callback = new SqlMapClientCallback(){
			public Object doInSqlMapClient(SqlMapExecutor executor) throws SQLException {
				executor.startBatch();
				for (int i=0;i < paramList.size();i++){
					DataParam dataRow = paramList.get(i);
					executor.insert(statementId, dataRow);
				}
				executor.executeBatch();
				return null;
			}
		};
		this.getSqlMapClientTemplate().execute(callback);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<DataRow> queryRecords(String statementId,DataParam param) throws DataAccessException {
		List result = new ArrayList();
		result = (List)this.getSqlMapClientTemplate().queryForList(statementId,param);
		return result;
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<DataRow> queryRecords(String statementId,Object id) throws DataAccessException {
		List result = new ArrayList();
		result = (List)this.getSqlMapClientTemplate().queryForList(statementId,id);
		return result;
	}

	public HashMap<String,DataRow> queryRecords(final String indexFieldName,String statementId,DataParam param) throws DataAccessException {
		final HashMap<String,DataRow> result = new HashMap<String,DataRow>();
		this.getSqlMapClientTemplate().queryWithRowHandler(statementId, param, new RowHandler() {
			@Override
			public void handleRow(Object valueObject) {
				DataRow row = (DataRow)valueObject;
				result.put(String.valueOf(row.get(indexFieldName)), row);
			}
		});
		return result;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public DataSet queryDataSet(String statementId,DataParam param) throws DataAccessException {
		DataSet result = new DataSet();
		List records = (List)this.getSqlMapClientTemplate().queryForList(statementId,param);
		result.setRecords(records);
		return result;
	}
	
	public DataBag queryDataBag(final String indexFieldName,String statementId,DataParam param) throws DataAccessException {
		final DataBag result = new DataBag();
		this.getSqlMapClientTemplate().queryWithRowHandler(statementId, param, new RowHandler() {
			@Override
			public void handleRow(Object valueObject) {
				DataRow row = (DataRow)valueObject;
				result.getDataRows().put(String.valueOf(row.get(indexFieldName)), row);
			}
		});
		return result;
	}
	
	public DataBox queryDataBox(final String indexFieldName,String statementId,DataParam param) throws DataAccessException {
		final DataBox result = new DataBox();
		this.getSqlMapClientTemplate().queryWithRowHandler(statementId, param, new RowHandler() {
			@Override
			public void handleRow(Object valueObject) {
				DataRow row = (DataRow)valueObject;
				String key = String.valueOf(row.get(indexFieldName));
				DataSet dataSet = getDataSet(result, key);
				dataSet.addDataRow(row);
			}
		});
		return result;
	}
	
	private DataSet getDataSet(DataBox dataBox,String key){
		if (!dataBox.getDataSets().containsKey(key)){
			DataSet dataSet = new DataSet();
			dataBox.addDataSet(key, dataSet);
		}
		return dataBox.getDataSet(key);
	}
	
	public DataRow getRecord(String statementId,DataParam param) throws DataAccessException {
		DataRow result = (DataRow)this.getSqlMapClientTemplate().queryForObject(statementId, param);
		return result;
	}
	public DataRow getRecord(String statementId,Object id) throws DataAccessException {
		DataRow result = (DataRow)this.getSqlMapClientTemplate().queryForObject(statementId, id);
		return result;
	}
	public void insertRecord(String statementId,DataParam param) throws DataAccessException {
		this.getSqlMapClientTemplate().insert(statementId, param);
	}
	public void updateRecord(String statementId,DataParam param) throws DataAccessException {
		this.getSqlMapClientTemplate().update(statementId, param);
	}
	public void deleteRecords(String statementId,Object id) throws DataAccessException {
		this.getSqlMapClientTemplate().delete(statementId, id);
	}
	public void deleteRecords(String statementId,DataParam param) throws DataAccessException {
		this.getSqlMapClientTemplate().delete(statementId, param);
	}
}
