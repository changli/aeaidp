package com.agileai.hotweb.common;

import java.util.HashMap;

import org.mvel2.templates.TemplateRuntime;

public class FormulaHelper {
	
	public static Object eval(String formaul,HashMap<String, Object> vars){
		Object result = null;
		result = TemplateRuntime.eval(formaul, vars);
		return result;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) {
		String formaul = "@{a+b}";
		HashMap vars = new HashMap();
		vars.put("a", 1);
		vars.put("b", 3);
		
		Object result = FormulaHelper.eval(formaul, vars);
		System.out.println(result);
	}
}
