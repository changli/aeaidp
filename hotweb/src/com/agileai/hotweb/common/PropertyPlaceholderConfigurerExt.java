package com.agileai.hotweb.common;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import com.agileai.hotweb.config.SharedConfig;
import com.agileai.util.CryptionUtil;
import com.agileai.util.StringUtil;

public class PropertyPlaceholderConfigurerExt extends PropertyPlaceholderConfigurer implements SharedConfig{
	private boolean useSharedConfig = false;
	private String datasouceSections = null;
	protected Properties properties = null;
	private List<String> datasouceSectionList = new ArrayList<String>(); 

	public void setLocations(Resource... locations) {
		Resource[] newLocations = new Resource[locations.length];
		for (int i=0;i < locations.length;i++){
			Resource resource = locations[i];
			if (resource.getFilename().equals("shared.properties")){
				String catalinaBase = System.getProperty("catalina.base");
				String filePath = catalinaBase+File.separator+"conf"+File.separator+resource.getFilename(); 
				File file = new File(filePath);
				FileSystemResource fsResource = new FileSystemResource(file);
				newLocations[i] = fsResource;
				
				this.useSharedConfig = true;
			}else{
				newLocations[i] = resource;
			}
		}
		super.setLocations(newLocations);
	}	
	
    @Override 
    protected void processProperties(ConfigurableListableBeanFactory beanFactory, Properties props) 
                    throws BeansException { 
		if (datasouceSectionList.isEmpty()){
			datasouceSectionList.add("dataSource");
		}
		for (int i=0;i < datasouceSectionList.size();i++){
			String datasourceSection = datasouceSectionList.get(i);
	    	String secretKey = props.getProperty(datasourceSection+".securityKey");
	        String password = props.getProperty(datasourceSection+".password"); 
	        if (password != null) { 
	        	props.setProperty(datasourceSection+".password", CryptionUtil.decryption(password, secretKey)); 
	        }			
		}
        this.properties = props;
        super.processProperties(beanFactory, props);
    }

	@Override
	public boolean isEnable() {
		return useSharedConfig;
	}

	@Override
	public String getConfig(String key) {
		return this.properties.getProperty(key);
	}

	public String getDatasouceSections() {
		return datasouceSections;
	}

	public void setDatasouceSections(String datasouceSections) {
		this.datasouceSections = datasouceSections;
		if (StringUtil.isNotNullNotEmpty(datasouceSections)){
			String[] sections = datasouceSections.split(",");
			this.datasouceSectionList.clear();
			for (int i=0;i < sections.length;i++){
				String section = sections[i];
				this.datasouceSectionList.add(section.trim());
			}
		}
	}

	@Override
	public String getConfig(String key, String defaultValue) {
		String result = getConfig(key);
		if (StringUtil.isNullOrEmpty(result)){
			result = defaultValue;
		}
		return result;
	}
}