package com.agileai.hotweb.common;

import redis.clients.jedis.JedisPoolConfig;

public class RedisConfig {
	private JedisPoolConfig jedisPoolConfig = null;
	private int timeout = 2000;
	private int socketTimeout = 2000;
	private int maxAttempts = 5;
	private String hosts = null;
	private String password = null;
	
	public RedisConfig(JedisPoolConfig poolConfig, String hosts,String password) {
		this.jedisPoolConfig = poolConfig;
		this.hosts = hosts;
		this.password = password;
	}

	public RedisConfig(JedisPoolConfig poolConfig, String hosts,String password,int timeout,int soTimeout,int maxAttempts) {
		this.jedisPoolConfig = poolConfig;
		this.hosts = hosts;
		this.password = password;
		this.timeout = timeout;
		this.socketTimeout = soTimeout;
		this.maxAttempts = maxAttempts;
	}
	
	public JedisPoolConfig getJedisPoolConfig() {
		return jedisPoolConfig;
	}

	public int getTimeout() {
		return timeout;
	}

	public String getHosts() {
		return hosts;
	}

	public String getPassword() {
		return password;
	}

	public int getSocketTimeout() {
		return socketTimeout;
	}

	public int getMaxAttempts() {
		return maxAttempts;
	}
}
