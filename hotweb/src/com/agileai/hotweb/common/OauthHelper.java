package com.agileai.hotweb.common;

import javax.servlet.http.HttpServletRequest;

import com.agileai.common.AppConfig;
import com.agileai.common.KeyGenerator;
import com.agileai.hotweb.config.OauthConfig;
import com.agileai.util.StringUtil;

public class OauthHelper {
	public AppConfig appConfig = null;
	public OauthHelper(AppConfig appConfig) {
		this.appConfig = appConfig;
	}
	
	public String buildOauthServiceId(String oauthSource) {
		String result = appConfig.getConfig("OauthConfigs", oauthSource+".serviceId");
		return result;
	}
	
	public OauthConfig buildOauthConfig(HttpServletRequest request,String oauthSource) {
		String oauthServerURL = appConfig.getConfig("OauthConfigs", oauthSource+".oauthServerURL");
		String oauthAppId = appConfig.getConfig("OauthConfigs", oauthSource+".oauthAppId");
		String oauthAppSecret = appConfig.getConfig("OauthConfigs", oauthSource+".oauthAppSecret");
		
		OauthConfig oauthConfig = new OauthConfig();
		oauthConfig.setOauthServerURL(oauthServerURL);
		oauthConfig.setAppId(oauthAppId);
		oauthConfig.setAppSecret(oauthAppSecret);
		oauthConfig.setOauthSource(oauthSource);
		
		String requestURL = request.getRequestURL().toString();
		String queryString = request.getQueryString();
		String callbackURL = requestURL;
		if (StringUtil.isNotNullNotEmpty(queryString)) {
			callbackURL = requestURL + "?" + queryString;
		}
		oauthConfig.setCallbackURL(callbackURL);
		String state = KeyGenerator.instance().genShortKey();
		oauthConfig.setState(state);
		
		return oauthConfig;
	}
}