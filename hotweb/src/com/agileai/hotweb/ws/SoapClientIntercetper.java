package com.agileai.hotweb.ws;

import java.util.List;

import javax.xml.namespace.QName;

import org.apache.cxf.binding.soap.SoapHeader;
import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.headers.Header;
import org.apache.cxf.helpers.DOMUtils;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.phase.Phase;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class SoapClientIntercetper extends AbstractSoapInterceptor {
	public static final String XML_NAMESPACEUR_ATT = "http://www.xcorp.com/ws";
	public static final String XML_AUTHENTICATION_EL = "auth";
	public static final String XML_USERID_EL = SoapServerIntercetper.XML_USERID_EL;
	public static final String XML_PASSWORD_EL = SoapServerIntercetper.XML_PASSWORD_EL;
	
	private String userId = null;
	private String password = null;
	
	public SoapClientIntercetper(String userId,String password) {
		super(Phase.WRITE);
		this.userId = userId;
		this.password = password;
	}
	
	public void handleMessage(SoapMessage message) throws Fault {
		QName qname = new QName("RequestSOAPHeader");

		Document doc = (Document) DOMUtils.createDocument();
		Element root = doc.createElementNS(XML_NAMESPACEUR_ATT,XML_AUTHENTICATION_EL);
		Element eUserId = doc.createElement(XML_USERID_EL);
		eUserId.setTextContent(userId);
		Element ePwd = doc.createElement(XML_PASSWORD_EL);
		ePwd.setTextContent(password);
		
		root.appendChild(eUserId);
		root.appendChild(ePwd);
		SoapHeader head = new SoapHeader(qname, root);
		List<Header> headers = message.getHeaders();
		headers.add(head);
	}
}