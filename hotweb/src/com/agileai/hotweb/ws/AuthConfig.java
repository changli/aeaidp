package com.agileai.hotweb.ws;

import java.util.LinkedHashMap;

import com.agileai.common.KeyValuePair;

public class AuthConfig {
	private String type = null;
	private String source = "LocalConfig";
	private LinkedHashMap<String,KeyValuePair> properties = new LinkedHashMap<String,KeyValuePair>();
	
	public static class Sources{
		public static final String LocalConfig = "LocalConfig";
		public static final String GlobalConfig = "GlobalConfig";
		public static final String HotServerConfig = "HotServerConfig";
		public static final String UMCServerConfig = "UMCServerConfig";
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public LinkedHashMap<String, KeyValuePair> getProperties() {
		return properties;
	}
	
	public void setProperties(LinkedHashMap<String, KeyValuePair> properties) {
		this.properties = properties;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}
}
