package com.agileai.hotweb.domain.core;

import java.io.Serializable;
import java.util.ArrayList;

public class OnlineUserList extends ArrayList<String> implements Serializable{
	private static final long serialVersionUID = 8830865679141847529L;
	public static final String AttributeKey = "__HotwebOnlineUserList__";
}
