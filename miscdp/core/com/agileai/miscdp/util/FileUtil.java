package com.agileai.miscdp.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Random;
import java.util.Vector;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class FileUtil {
	private static final int DEFAULT_READING_SIZE = 8192;

	public static void copyFile(File sourceFile, File destFile)
			throws IOException {
		if (!destFile.exists()) {
			File parent = destFile.getParentFile();
			if ((parent != null) && (!parent.exists())) {
				parent.mkdirs();
			}

			destFile.createNewFile();
		}
		InputStream in = null;
		OutputStream out = null;
		try {
			in = new FileInputStream(sourceFile);
			out = new FileOutputStream(destFile);
			copy(in, out);
		} finally {
			if (in != null)
				in.close();
			if (out != null)
				out.close();
		}
	}

	public static void copy(InputStream in, OutputStream out)
			throws IOException {
		byte[] buf = new byte[1024];
		int len = 0;
		while ((len = in.read(buf, 0, buf.length)) != -1)
			out.write(buf, 0, len);
	}

	public static void writeFile(File file, byte[] contents) throws IOException {
		if (!file.exists()) {
			file.createNewFile();
		}
		if (!file.isFile()) {
			throw new IOException("File to be written not exist, file path : "
					+ file.getAbsolutePath());
		}
		FileOutputStream fileOut = null;
		BufferedOutputStream bOutput = null;
		try {
			fileOut = new FileOutputStream(file);
			bOutput = new BufferedOutputStream(fileOut);
			bOutput.write(contents);
			bOutput.flush();
		} finally {
			if (fileOut != null)
				try {
					fileOut.close();
				} catch (IOException localIOException1) {
					throw localIOException1;
				}
			if (bOutput != null)
				try {
					bOutput.close();
				} catch (IOException localIOException2) {
					throw localIOException2;
				}
		}
	}

	public static byte[] readFileContent(File file) throws IOException {
		if ((!file.exists()) || (!file.isFile())) {
			throw new IOException("File to be readed not exist, file path : "
					+ file.getAbsolutePath());
		}
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		FileInputStream in = new FileInputStream(file);
		byte[] contents = (byte[]) null;
		try {
			copy(in, out);
			contents = out.toByteArray();
		} catch (IOException e) {
			throw e;
		} finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException localIOException1) {
				}
		}
		return contents;
	}

	public static String readFileContentAsString(File file) throws IOException {
		return readFileContentAsString(file, null);
	}

	public static String readFileContentAsString(File file, String charsetName)
			throws IOException {
		if ((!file.exists()) || (!file.isFile())) {
			throw new IOException("File to be readed not exist, file path : "
					+ file.getAbsolutePath());
		}

		FileInputStream fileIn = null;
		InputStreamReader inReader = null;
		BufferedReader bReader = null;
		try {
			fileIn = new FileInputStream(file);
			inReader = charsetName == null ? new InputStreamReader(fileIn)
					: new InputStreamReader(fileIn, charsetName);
			bReader = new BufferedReader(inReader);
			StringBuffer content = new StringBuffer();
			char[] chBuffer = new char[1024];
			int readedNum = -1;
			while ((readedNum = bReader.read(chBuffer)) != -1) {
				content.append(chBuffer, 0, readedNum);
			}

			String str = content.toString();
			return str;
		} finally {
			if (fileIn != null)
				try {
					fileIn.close();
				} catch (IOException localIOException3) {
				}
			if (bReader != null)
				try {
					bReader.close();
				} catch (IOException localIOException4) {
				}
		}
	}

	public static void writeStringAsFileContent(File file, String content)
			throws IOException {
		writeStringAsFileContent(file, content, null);
	}

	public static void writeStringAsFileContent(File file, String content,
			String charsetName) throws IOException {
		if ((!file.exists()) || (!file.isFile())) {
			throw new IOException("File to be written not exist, file path : "
					+ file.getAbsolutePath());
		}

		FileOutputStream fileOut = null;
		OutputStreamWriter outWriter = null;
		BufferedWriter bWriter = null;
		try {
			fileOut = new FileOutputStream(file);
			outWriter = charsetName == null ? new OutputStreamWriter(fileOut)
					: new OutputStreamWriter(fileOut, charsetName);
			bWriter = new BufferedWriter(outWriter);
			bWriter.write(content);
			bWriter.flush();
		} finally {
			if (fileOut != null)
				try {
					fileOut.close();
				} catch (IOException localIOException1) {
				}
			if (bWriter != null)
				try {
					bWriter.close();
				} catch (IOException localIOException2) {
				}
		}
	}

	public static void deleteWholeDirectory(File dir) throws IOException {
		if ((!dir.exists()) || (!dir.isDirectory())) {
			throw new IOException(
					"Directory to be deleted not exist, directory path : "
							+ dir.getAbsolutePath());
		}

		File[] children = dir.listFiles();
		for (File child : children) {
			if (child.isDirectory())
				deleteWholeDirectory(child);
			else {
				child.delete();
			}
		}

		dir.delete();
	}

	public static void deleteFilesInWholeDirectory(File dir,
			FilenameFilter filter) throws IOException {
		if ((!dir.exists()) || (!dir.isDirectory())) {
			throw new IOException(
					"Directory to be deleted not exist, directory path : "
							+ dir.getAbsolutePath());
		}

		File[] children = dir.listFiles(filter);
		for (File child : children) {
			if (child.isDirectory())
				deleteFilesInWholeDirectory(child, filter);
			else {
				child.delete();
			}
		}

		dir.delete();
	}

	public static String getStringFromXMLFile(String xmlFilePath)
			throws IOException {
		File file = new File(xmlFilePath);
		return getStringFromXMLFile(file);
	}

	public static String getStringFromXMLFile(File xmlFile) throws IOException {
		String temp = "";
		String result = "";
		if (xmlFile.exists()) {
			BufferedReader reader = null;
			try {
				reader = new BufferedReader(new FileReader(xmlFile));
				while ((temp = reader.readLine()) != null)
					result = result + temp;
			} catch (FileNotFoundException localFileNotFoundException) {
				throw new FileNotFoundException("file not found");
			} catch (IOException localIOException1) {
				throw new IOException();
			} finally {
				try {
					reader.close();
					reader = null;
				} catch (IOException localIOException2) {
				}
			}
			return result;
		}
		return null;
	}

	public static String getFileNameFromPath(String filePath, boolean isExt) {
		File file = new File(filePath);
		return getFileNameFromPath(file, isExt);
	}

	public static String getFileNameFromPath(String filePath) {
		File file = new File(filePath);
		return getFileNameFromPath(file, false);
	}

	public static String getFileNameFromPath(File file, boolean isExt) {
		String filePath = file.getAbsolutePath();

		int begin = filePath.lastIndexOf(File.separator);
		int end = filePath.lastIndexOf(".");

		String fileName = null;

		if ((end == -1) || (end < begin)) {
			fileName = filePath.substring(begin + 1);
		} else if (isExt)
			fileName = filePath.substring(begin + 1);
		else {
			fileName = filePath.substring(begin + 1, end);
		}

		return fileName;
	}

	public static boolean exists(URL url) {
		if (url.getProtocol().equals("file")) {
			File f = new File(url.getFile());
			return f.exists();
		}

		if (url.getProtocol().equals("jar")) {
			String spec = url.getFile();

			if (spec.startsWith("file:")) {
				int separator = spec.indexOf('!');
				if (separator == -1) {
					return false;
				}

				String jarFileName = spec.substring(5, separator++);
				String entryName = null;

				separator++;
				if (separator != spec.length())
					entryName = spec.substring(separator, spec.length());
				else {
					return false;
				}
				try {
					JarFile jarFile = new JarFile(jarFileName);
					JarEntry jarEntry = (JarEntry) jarFile.getEntry(entryName);
					jarFile.close();
					return jarEntry != null;
				} catch (IOException localIOException1) {
					return false;
				}
			}
		}

		try {
			InputStream is = url.openStream();
			is.close();
			return true;
		} catch (IOException localIOException2) {
		}
		return false;
	}

	public static void copyFilesIngoreHiddenFiles(File source, File dest)
			throws IOException {
		if ((source != null) && (dest != null)
				&& (!source.getName().startsWith("."))) {
			copyFile2(source, dest);

			if (source.isDirectory()) {
				File[] list = source.listFiles();
				if (list != null)
					for (File file : list)
						copyFilesIngoreHiddenFiles(file,
								new File(dest.getAbsolutePath()
										+ File.separator + file.getName()));
			}
		}
	}

	public static void copyFolder(File srcDir, File destDir,
			List<?> excludedList) throws IOException {
		if ((destDir == null) || (!destDir.exists())) {
			throw new FileNotFoundException("File destDir not existed");
		}
		if ((srcDir == null) || (!srcDir.exists())) {
			throw new FileNotFoundException("File srcDir not existed");
		}
		File[] files = srcDir.listFiles();
		for (int i = 0; i < files.length; i++) {
			if ((excludedList != null) && (excludedList.contains(files[i]))) {
				continue;
			}
			String fileName = files[i].getName();
			if (files[i].isFile()) {
				copyFileToDir(files[i], destDir);
			} else {
				File file = new File(destDir, fileName);
				file.mkdir();
				copyFolder(files[i], file, excludedList);
			}
		}
	}

	public static void copyFileToDir(File sourceFile, File destDir)
			throws IOException {
		String name = sourceFile.getName();
		File destFile = new File(destDir, name);
		if (sourceFile.isDirectory()) {
			destFile.mkdirs();
			return;
		}
		InputStream in = null;
		OutputStream out = null;
		try {
			in = new FileInputStream(sourceFile);
			out = new FileOutputStream(destFile);
			copy(in, out);
		} finally {
			if (in != null)
				in.close();
			if (out != null)
				out.close();
		}
	}

	public static void copyFile2(File sourceFile, File destFile)
			throws IOException {
		if (sourceFile.isDirectory()) {
			destFile.mkdirs();
			return;
		}
		InputStream in = null;
		OutputStream out = null;
		try {
			in = new FileInputStream(sourceFile);
			out = new FileOutputStream(destFile);
			copy(in, out);
		} finally {
			if (in != null)
				in.close();
			if (out != null)
				out.close();
		}
	}

	public static void copyFile(File sourceFile, File destDir,
			String newFileName) throws IOException {
		File destFile = new File(destDir, newFileName);
		if (sourceFile.isDirectory()) {
			destFile.mkdirs();
			return;
		}
		InputStream in = null;
		OutputStream out = null;
		try {
			in = new FileInputStream(sourceFile);
			out = new FileOutputStream(destFile);
			copy(in, out);
		} finally {
			if (in != null)
				in.close();
			if (out != null)
				out.close();
		}
	}

	public static byte[] toByteArray(String path) throws IOException {
		File file = new File(path);
		FileInputStream in = null;
		try {
			in = new FileInputStream(file);
			byte[] data = new byte[(int) file.length()];
			in.read(data);
			byte[] arrayOfByte1 = data;
			return arrayOfByte1;
		} finally {
			in.close();
		}
	}

	public static void copyDir(File sourceDir, File destDir,
			String excludedDirName) throws IOException {
		copyDir(sourceDir, destDir, true, excludedDirName);
	}

	public static void copyDir(File from, File to) throws IOException {
		copyDir(from, to, true,"");
	}

	public static void copyDir(File from, File to, boolean includeSubdirs)
			throws IOException {
		copyDir(from, to, includeSubdirs, "");
	}

	public static void copyDir(File from, File to, boolean includeSubdirs,
			List<String> excludedDirNames) throws IOException {
		if (!to.exists()) {
			to.mkdirs();
		}
		if ((from == null) || (!from.isDirectory()) || (!to.isDirectory()))
			return;
		File[] fs = from.listFiles();
		if (fs == null)
			return;
		for (int i = 0; i < fs.length; i++) {
			String n = fs[i].getName();
			File c = new File(to, n);
			if ((fs[i].isDirectory()) && (!includeSubdirs))
				continue;
			if (fs[i].isDirectory()) {
				if (excludedDirNames.contains(fs[i].getName())) {
					continue;
				}
				c.mkdirs();
				copyDir(fs[i], c, includeSubdirs, excludedDirNames);
			} else {
				copyFileToDir(fs[i], to);
			}
		}
	}	
	
	public static void copyWebRoot(File from, File to, boolean includeSubdirs,
			List<String> excludedDirNames) throws IOException {
		if (!to.exists()) {
			to.mkdirs();
		}
		if ((from == null) || (!from.isDirectory()) || (!to.isDirectory()))
			return;
		File[] fs = from.listFiles();
		if (fs == null)
			return;
		for (int i = 0; i < fs.length; i++) {
			String n = fs[i].getName();
			File c = new File(to, n);
			if ((fs[i].isDirectory()) && (!includeSubdirs))
				continue;
			if (fs[i].isDirectory()) {
				if (excludedDirNames.contains(fs[i].getName())) {
					continue;
				}
				c.mkdirs();
				copyDir(fs[i], c, includeSubdirs, excludedDirNames);
			} else {
				copyFileToDir(fs[i], to);
			}
		}
	}		
	
	public static void copyDir(File from, File to, boolean includeSubdirs,
			String excludedDirName) throws IOException {
		if (!to.exists()) {
			to.mkdirs();
		}
		if ((from == null) || (!from.isDirectory()) || (!to.isDirectory()))
			return;
		File[] fs = from.listFiles();
		if (fs == null)
			return;
		for (int i = 0; i < fs.length; i++) {
			String n = fs[i].getName();
			File c = new File(to, n);
			if ((fs[i].isDirectory()) && (!includeSubdirs))
				continue;
			if (fs[i].isDirectory()) {
				if (fs[i].getName().equals(excludedDirName)) {
					continue;
				}
				c.mkdirs();
				copyDir(fs[i], c, includeSubdirs, excludedDirName);
			} else {
				copyFileToDir(fs[i], to);
			}
		}
	}

	public static File makeTempDir(File parentDir, String prefix, String suffix)
			throws IOException {
		if (suffix == null)
			suffix = ".tmp";
		File tmpdir = null;
		if (parentDir == null)
			tmpdir = new File(System.getProperty("java.io.tmpdir", "/temp"));
		else {
			tmpdir = parentDir;
		}
		int counter = new Random().nextInt() & 0xFFFF;
		File f;
		do {
			counter++;
			f = new File(tmpdir, prefix + Integer.toString(counter) + suffix);
		} while ((f.exists()) || (!f.mkdirs()));
		return f;
	}

	public static File makeTempDir(String parentDir, String prefix,
			String suffix) throws IOException {
		File fileParentDir = null;
		if (parentDir != null)
			fileParentDir = new File(System.getProperty("java.io.tmpdir",
					"/temp"), parentDir);
		return makeTempDir(fileParentDir, prefix, suffix);
	}

	public static File getTempFile(File parentDir) {
		if (parentDir == null)
			parentDir = new File(System.getProperty("java.io.tmpdir", "/temp"));
		int counter = new Random().nextInt() & 0xFFFF;
		File f;
		do {
			counter++;
			f = new File(parentDir, "tempfile" + Integer.toString(counter));
		} while (f.exists());
		return f;
	}

	public static File getTempFolder() {
		File parentDir = new File(System.getProperty("java.io.tmpdir", "/temp"));
		int counter = new Random().nextInt() & 0xFFFF;
		File f = null;
		do {
			counter++;
			f = new File(parentDir, "tempfolder" + Integer.toString(counter));
		} while (f.exists());
		f.mkdir();
		return f;
	}

	public static void unpackZipFile(File zipFile, File dir) throws IOException {
		InputStream fis = new FileInputStream(zipFile);
		ZipInputStream zis = new ZipInputStream(fis);
		try {
			for (ZipEntry e = zis.getNextEntry(); e != null; e = zis
					.getNextEntry()) {
				File f = new File(dir, e.getName());
				if (e.isDirectory()) {
					if ((!f.exists()) && (!f.mkdirs()))
						throw new IOException("Cannot make directory "
								+ f.getPath());
				} else {
					File d = f.getParentFile();
					if ((d != null) && (!d.exists()) && (!d.mkdirs())) {
						throw new IOException("Cannot make directory "
								+ d.getPath());
					}

					OutputStream out = new FileOutputStream(f);
					copy(zis, out);
					out.close();
				}
			}
		} finally {
			zis.close();
			fis.close();
		}
	}

	public static void makeJarFile(File jarFile, File dir) throws IOException {
		makeJarFile(jarFile, dir, null);
	}

	public static void makeJarFile(File jarFile, File dir, Vector excludedList)
			throws IOException {
		OutputStream fos = new FileOutputStream(jarFile);
		ZipOutputStream zos = new ZipOutputStream(fos);
		try {
			addToJar(zos, dir, excludedList, new Vector());
		} finally {
			zos.close();
			fos.close();
		}
	}

	public static void addToJar(ZipOutputStream zos, File dir,
			Vector excludedList, Vector entriesAdded) throws IOException {
		Vector v = new Vector();
		expand(dir, dir.list(), excludedList, v);

		String path = dir.getPath();
		if (!path.endsWith(File.separator)) {
			path = path + File.separator;
		}
		path = path.replace(File.separatorChar, '/');
		for (int i = 0; i < v.size(); i++)
			addFile(zos, path, (File) v.elementAt(i), entriesAdded);
	}

	private static void expand(File dir, String[] files, Vector excludeList,
			Vector v) {
		if (files == null) {
			return;
		}
		for (int i = 0; i < files.length; i++) {
			File f = new File(dir, files[i]);
			if ((excludeList == null) || (!excludeList.contains(f)))
				if (f.isFile()) {
					v.addElement(f);
				} else if (f.isDirectory()) {
					v.addElement(f);
					expand(f, f.list(), excludeList, v);
				}
		}
	}

	private static void addFile(ZipOutputStream zos, String path, File file,
			Vector entriesAdded) throws IOException {
		String name = file.getPath();
		boolean isDir = file.isDirectory();

		if ((isDir) && (!name.endsWith(File.separator))) {
			name = name + File.separator;
		}

		name = name.replace(File.separatorChar, '/');
		if (name.startsWith(path)) {
			name = name.substring(path.length());
		}
		if (entriesAdded.contains(name)) {
			return;
		}

		long size = isDir ? 0L : file.length();
		ZipEntry e = new ZipEntry(name);
		e.setTime(file.lastModified());
		if (size == 0L) {
			e.setMethod(0);
			e.setSize(0L);
			e.setCrc(0L);
		}
		zos.putNextEntry(e);
		if (!isDir) {
			InputStream in = new FileInputStream(file);
			copy(in, zos);
			in.close();
		}
		zos.closeEntry();
		entriesAdded.addElement(name);
	}

	public static String getFileNameWithoutExt(String filePath) {
		filePath = filePath.replace('\\', '/');
		int pos = filePath.lastIndexOf('/');
		String fileName = filePath.substring(pos + 1);
		int pos2 = fileName.lastIndexOf('.');
		if (pos2 == -1) {
			return fileName;
		}
		return fileName.substring(0, pos2);
	}

	public static boolean isZipFile(File file) {
		try {
			ZipFile zip = new ZipFile(file);
			zip.close();
			return true;
		} catch (IOException localIOException) {
			localIOException.printStackTrace();
		}
		return false;
	}

	public static void removeDir(File dir) {
		if (dir.isDirectory()) {
			removeFile(dir);
		}
		dir.delete();
	}

	public static void removeFile(File file) {
		if (!file.isFile()) {
			if (file.isDirectory()) {
				String[] list = file.list();
				if (list != null)
					for (int i = 0; i < list.length; i++) {
						File f = new File(file, list[i]);
						removeFile(f);
					}
			}
		}
		file.delete();
	}

	public static URL getFileURL(File file) {
		try {
			file = file.getCanonicalFile();
		} catch (IOException localIOException) {
			localIOException.printStackTrace();
		}
		String path = file.getAbsolutePath();
		if (File.separatorChar != '/') {
			path = path.replace(File.separatorChar, '/');
		}
		if (!path.startsWith("/")) {
			path = "/" + path;
		}
		if ((!path.endsWith("/")) && (file.isDirectory()))
			path = path + "/";
		try {
			return new URL("file", "", -1, path);
		} catch (MalformedURLException localMalformedURLException) {
		}
		throw new InternalError();
	}

	public static String getFileStringContent(File file) throws IOException {
		return new String(getFileByteContent(file));
	}

	public static String getFileStringContent(File file, String charsetName)
			throws IOException {
		return new String(getFileByteContent(file), charsetName);
	}

	public static String getInputStreamStringContent(InputStream inputStream)
			throws IOException {
		return new String(getInputStreamAsByteArray(inputStream, -1));
	}

	public static byte[] getFileByteContent(File file) throws IOException {
		InputStream stream = null;
		try {
			stream = new BufferedInputStream(new FileInputStream(file));
			byte[] arrayOfByte = getInputStreamAsByteArray(stream,
					(int) file.length());
			return arrayOfByte;
		} finally {
			if (stream != null)
				try {
					stream.close();
				} catch (IOException localIOException2) {
					throw localIOException2;
				}
		}
	}

	public static byte[] getInputStreamAsByteArray(InputStream stream,
			int length) throws IOException {
		byte[] contents;
		if (length == -1) {
			contents = new byte[0];
			int contentsLength = 0;
			int amountRead = -1;
			do {
				int amountRequested = Math.max(stream.available(), 8192);

				if (contentsLength + amountRequested > contents.length) {
					System.arraycopy(contents, 0,
							contents = new byte[contentsLength
									+ amountRequested], 0, contentsLength);
				}

				amountRead = stream.read(contents, contentsLength,
						amountRequested);

				if (amountRead <= 0)
					continue;
				contentsLength += amountRead;
			} while (amountRead != -1);

			if (contentsLength < contents.length)
				System.arraycopy(contents, 0,
						contents = new byte[contentsLength], 0, contentsLength);
		} else {
			contents = new byte[length];
			int len = 0;
			int readSize = 0;
			while ((readSize != -1) && (len != length)) {
				len += readSize;
				readSize = stream.read(contents, len, length - len);
			}
		}

		return contents;
	}
}