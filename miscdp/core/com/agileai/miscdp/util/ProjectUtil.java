package com.agileai.miscdp.util;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
/**
 * 工程辅助类
 */
public class ProjectUtil {
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static IResource[] getSelectedResources(ISelection selection) {
		ArrayList resources = null;
		if (!selection.isEmpty()) {
			resources = new ArrayList();
			Iterator elements = ((IStructuredSelection) selection).iterator();
			while (elements.hasNext()) {
				Object next = elements.next();
				if (next instanceof IResource) {
					resources.add(next);
					continue;
				}
				if (next instanceof IAdaptable) {
					IAdaptable a = (IAdaptable) next;
					Object adapter = a.getAdapter(IResource.class);
					if (adapter instanceof IResource) {
						resources.add(adapter);
						continue;
					}
				}
			}
		}
		if (resources != null && !resources.isEmpty()) {
			IResource[] result = new IResource[resources.size()];
			resources.toArray(result);
			return result;
		}
		return new IResource[0];
	}
	
	public static IResource getSelectedResource(ISelection selection) {
		IResource[] resources = getSelectedResources(selection);
		if (resources != null && resources.length >= 1) {
			return resources[0];
		}
		return null;
	}
	public static IClasspathEntry[] getClasspathEntry(IProject project) {
		IClasspathEntry[] classpathEntries = null;
		try {
			IJavaProject javaProject = JavaCore.create(project);
			classpathEntries = javaProject.getRawClasspath();
		} catch (JavaModelException e) {
			e.printStackTrace();
		}
		return classpathEntries;
	}
}
