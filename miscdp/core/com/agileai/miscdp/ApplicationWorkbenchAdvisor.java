package com.agileai.miscdp;

import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.application.IWorkbenchConfigurer;
import org.eclipse.ui.internal.ide.application.IDEWorkbenchAdvisor;

public class ApplicationWorkbenchAdvisor extends IDEWorkbenchAdvisor
{
  public void initialize(IWorkbenchConfigurer configurer)
  {
    super.initialize(configurer);
    configurer.setSaveAndRestore(true);
    PlatformUI.getPreferenceStore().setValue("SHOW_TRADITIONAL_STYLE_TABS", false);
    PlatformUI.getPreferenceStore().setValue("DOCK_PERSPECTIVE_BAR", "topRight");
  }

  public String getInitialWindowPerspectiveId()
  {
    return DeveloperConst.PERSPECTIVE_ID;
  }
}