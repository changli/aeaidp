package com.agileai.miscdp.classpath;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jdt.core.IClasspathContainer;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;

import com.agileai.miscdp.DeveloperConst;
import com.agileai.miscdp.MiscdpPlugin;
import com.agileai.miscdp.hotweb.domain.ProjectConfig;
import com.agileai.miscdp.hotweb.domain.ProjectConfig.ProjectType;
import com.agileai.miscdp.util.MiscdpUtil;

public class HotServerContainer implements IClasspathContainer {
	public static IPath ID = new Path(DeveloperConst.HOTSERVER_CONTAINER_ID);
    private String desc;
    private IPath targetPath;
    private File dir;
    private HashSet<String> exts;
    private ProjectConfig projectConfig = null;
  
    private FilenameFilter dirFilter = new FilenameFilter() {
        public boolean accept(File dir, String name) {
        	if (name.length() > 4){
        		String nameExt = name.substring(name.length()-3,name.length());
                if(exts.contains(nameExt)) {
                    return true;
                }else{
                	return false;
                }
        	}
            return false;
        }
    };
    
    public HotServerContainer(IPath path, IJavaProject project) {
        targetPath = path;
        exts = new HashSet<String>();
        this.exts.add("jar");
        this.exts.add("zip");
        dir = new File(getHotServerLibraryDir()); 
        desc = "HotServer Container Libraries";
        
        String appName = project.getElementName();
        this.projectConfig = MiscdpUtil.getProjectConfig(appName);
        
    }
    
    public boolean isValid() {
        if(dir.exists() && dir.isDirectory()) {
            return true;
        }
        return false;
    }
    
    public static String getHotServerLibraryDir(){
    	try {
			String reponsitory = FileLocator.toFileURL(Platform.getBundle(MiscdpPlugin.getPluginId()).getResource("resource")).getFile();
			return reponsitory.substring(1)+"libs";
		} catch (IOException e) {
			MiscdpPlugin.getDefault().logError(e.getLocalizedMessage(), e);
		}
    	return "";
    }
    
    public IClasspathEntry[] getClasspathEntries() {
        ArrayList<IClasspathEntry> entryList = new ArrayList<IClasspathEntry>();
        File[] libs = dir.listFiles(dirFilter);
        for( File lib: libs ) {
        	String name = lib.getName();
            String ext = name.substring(name.length()-3,name.length()); 
            File srcArc = new File(lib.getAbsolutePath().replace("."+ext, "-src."+ext));
            Path srcPath = null;
            if( srcArc.exists()) {
                srcPath = new Path(srcArc.getAbsolutePath());
            }
            entryList.add(JavaCore.newLibraryEntry(new Path(lib.getAbsolutePath()) , srcPath, new Path("/")));                
        }
        
        if (ProjectType.BPMWebProject.equals(this.projectConfig.getProjectType())){
        	File bpmDir = new File(getHotServerLibraryDir()+File.separator+"bpm");
        	File[] bpmLibs = bpmDir.listFiles(dirFilter);
        	for( File lib: bpmLibs ) {
            	String name = lib.getName();
                String ext = name.substring(name.length()-3,name.length()); 
                File srcArc = new File(lib.getAbsolutePath().replace("."+ext, "-src."+ext));
                Path srcPath = null;
                if( srcArc.exists()) {
                    srcPath = new Path(srcArc.getAbsolutePath());
                }
                entryList.add(JavaCore.newLibraryEntry(new Path(lib.getAbsolutePath()) , srcPath, new Path("/")));                
            }	
        }
        IClasspathEntry[] entryArray = new IClasspathEntry[entryList.size()];
        return (IClasspathEntry[])entryList.toArray(entryArray);
    }
    
    public String getDescription() {
        return desc;
    }
    
    public int getKind() {
        return IClasspathContainer.K_APPLICATION;
    }    
    
    public IPath getPath() {
        return targetPath;
    }
    
    public File getDir() {
        return dir;
    }
    
    public boolean isContained(File file) {
        if(file.getParentFile().equals(dir)) {
            String fExt = file.toString().substring(file.toString().lastIndexOf('.') + 1);
            if(exts.contains(fExt.toLowerCase())) {
                return true;
            }
        }        
        return false;
    }    
}