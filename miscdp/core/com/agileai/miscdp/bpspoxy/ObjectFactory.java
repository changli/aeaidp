
package com.agileai.miscdp.bpspoxy;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.agileai.miscdp.bpspoxy package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RetrieveFuncModelList_QNAME = new QName("http://www.bps.com/service", "retrieveFuncModelList");
    private final static QName _DeployFuncModelZip_QNAME = new QName("http://www.bps.com/service", "deployFuncModelZip");
    private final static QName _RetrieveFuncModelListResponse_QNAME = new QName("http://www.bps.com/service", "retrieveFuncModelListResponse");
    private final static QName _ExportFuncModelZipResponse_QNAME = new QName("http://www.bps.com/service", "exportFuncModelZipResponse");
    private final static QName _ExportFuncModelZip_QNAME = new QName("http://www.bps.com/service", "exportFuncModelZip");
    private final static QName _ResultStatus_QNAME = new QName("http://www.bps.com/service", "ResultStatus");
    private final static QName _DeployFuncModelZipResponse_QNAME = new QName("http://www.bps.com/service", "deployFuncModelZipResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.agileai.miscdp.bpspoxy
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RetrieveFuncModelList }
     * 
     */
    public RetrieveFuncModelList createRetrieveFuncModelList() {
        return new RetrieveFuncModelList();
    }

    /**
     * Create an instance of {@link ExportFuncModelZipResponse }
     * 
     */
    public ExportFuncModelZipResponse createExportFuncModelZipResponse() {
        return new ExportFuncModelZipResponse();
    }

    /**
     * Create an instance of {@link ModelField }
     * 
     */
    public ModelField createModelField() {
        return new ModelField();
    }

    /**
     * Create an instance of {@link ExportFuncModelZip }
     * 
     */
    public ExportFuncModelZip createExportFuncModelZip() {
        return new ExportFuncModelZip();
    }

    /**
     * Create an instance of {@link RetrieveFuncModelListResponse }
     * 
     */
    public RetrieveFuncModelListResponse createRetrieveFuncModelListResponse() {
        return new RetrieveFuncModelListResponse();
    }

    /**
     * Create an instance of {@link DeployFuncModelZip }
     * 
     */
    public DeployFuncModelZip createDeployFuncModelZip() {
        return new DeployFuncModelZip();
    }

    /**
     * Create an instance of {@link FuncModelZip }
     * 
     */
    public FuncModelZip createFuncModelZip() {
        return new FuncModelZip();
    }

    /**
     * Create an instance of {@link DeployFuncModelZipResponse }
     * 
     */
    public DeployFuncModelZipResponse createDeployFuncModelZipResponse() {
        return new DeployFuncModelZipResponse();
    }

    /**
     * Create an instance of {@link ResultStatus }
     * 
     */
    public ResultStatus createResultStatus() {
        return new ResultStatus();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveFuncModelList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bps.com/service", name = "retrieveFuncModelList")
    public JAXBElement<RetrieveFuncModelList> createRetrieveFuncModelList(RetrieveFuncModelList value) {
        return new JAXBElement<RetrieveFuncModelList>(_RetrieveFuncModelList_QNAME, RetrieveFuncModelList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeployFuncModelZip }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bps.com/service", name = "deployFuncModelZip")
    public JAXBElement<DeployFuncModelZip> createDeployFuncModelZip(DeployFuncModelZip value) {
        return new JAXBElement<DeployFuncModelZip>(_DeployFuncModelZip_QNAME, DeployFuncModelZip.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveFuncModelListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bps.com/service", name = "retrieveFuncModelListResponse")
    public JAXBElement<RetrieveFuncModelListResponse> createRetrieveFuncModelListResponse(RetrieveFuncModelListResponse value) {
        return new JAXBElement<RetrieveFuncModelListResponse>(_RetrieveFuncModelListResponse_QNAME, RetrieveFuncModelListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExportFuncModelZipResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bps.com/service", name = "exportFuncModelZipResponse")
    public JAXBElement<ExportFuncModelZipResponse> createExportFuncModelZipResponse(ExportFuncModelZipResponse value) {
        return new JAXBElement<ExportFuncModelZipResponse>(_ExportFuncModelZipResponse_QNAME, ExportFuncModelZipResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExportFuncModelZip }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bps.com/service", name = "exportFuncModelZip")
    public JAXBElement<ExportFuncModelZip> createExportFuncModelZip(ExportFuncModelZip value) {
        return new JAXBElement<ExportFuncModelZip>(_ExportFuncModelZip_QNAME, ExportFuncModelZip.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResultStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bps.com/service", name = "ResultStatus")
    public JAXBElement<ResultStatus> createResultStatus(ResultStatus value) {
        return new JAXBElement<ResultStatus>(_ResultStatus_QNAME, ResultStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeployFuncModelZipResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bps.com/service", name = "deployFuncModelZipResponse")
    public JAXBElement<DeployFuncModelZipResponse> createDeployFuncModelZipResponse(DeployFuncModelZipResponse value) {
        return new JAXBElement<DeployFuncModelZipResponse>(_DeployFuncModelZipResponse_QNAME, DeployFuncModelZipResponse.class, null, value);
    }

}
