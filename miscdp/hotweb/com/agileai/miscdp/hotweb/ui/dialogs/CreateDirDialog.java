package com.agileai.miscdp.hotweb.ui.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.agileai.miscdp.hotweb.ui.views.FuncModelTreeView;
import com.agileai.miscdp.ui.TextCheckPolicy;
import com.agileai.miscdp.util.DialogUtil;
import com.agileai.miscdp.util.DialogUtil.MESSAGE_TYPE;
import com.agileai.miscdp.util.MiscdpUtil;
/**
 * 创建目录提示框
 */
public class CreateDirDialog extends Dialog {
	private String funcPId = "0";
	private String funcPName = null;
	private Text parentText;
	private Text moduleNameText;
	private Text funcSubPkgText;
	
	private FuncModelTreeView funcTree = null;
	
	private String funcId;
	private String funcName = null;
	private String funcSubPkg = null;
	
	public CreateDirDialog(FuncModelTreeView funcTree,Shell parentShell) {
		super(parentShell);
		this.funcTree = funcTree;
	}
	public String getFuncPId() {
		return funcPId;
	}
	public void setFuncPId(String funcPId) {
		this.funcPId = funcPId;
	}
	public void setFuncPName(String funcPName) {
		this.funcPName = funcPName;
	}
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.marginTop = 20;
		gridLayout.numColumns = 2;
		gridLayout.marginLeft = 10;
		gridLayout.verticalSpacing = 2;
		gridLayout.marginWidth = 2;
		container.setLayout(gridLayout);

		final Label parentNodeLabel = new Label(container, SWT.NONE);
		String parentNodeName = MiscdpUtil.getIniReader().getValue(CreateDirDialog.class.getSimpleName(),"ParentNode");
		parentNodeLabel.setText(parentNodeName);

		parentText = new Text(container, SWT.READ_ONLY | SWT.BORDER);
		parentText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		parentText.setText(this.funcPName);
		
		Label lblNewLabel = new Label(container, SWT.NONE);
		lblNewLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		String moduleCode = MiscdpUtil.getIniReader().getValue(CreateDirDialog.class.getSimpleName(),"ModuleCode");
		lblNewLabel.setText(moduleCode);
		
		funcSubPkgText = new Text(container, SWT.BORDER);
		funcSubPkgText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new TextCheckPolicy(funcSubPkgText, TextCheckPolicy.POLICY_PACKAGE_NAME);
		funcSubPkgText.setFocus();
		
		final Label label = new Label(container, SWT.HORIZONTAL);
		String moduleName = MiscdpUtil.getIniReader().getValue(CreateDirDialog.class.getSimpleName(),"ModuleName");
		label.setText(moduleName);

		moduleNameText = new Text(container, SWT.BORDER);
		moduleNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		return container;
	}
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,true);
		createButton(parent, IDialogConstants.CANCEL_ID,IDialogConstants.CANCEL_LABEL, false);
	}
	@Override
	protected Point getInitialSize() {
		return new Point(399, 226);
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		String createDirDialogTitle = MiscdpUtil.getIniReader().getValue(CreateDirDialog.class.getSimpleName(), "CreateDirDialogTitle"); 
		newShell.setText(createDirDialogTitle);
	}
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.CANCEL_ID) {
			this.close();
			this.setReturnCode(-1);
			return;
		}
		if (buttonId == IDialogConstants.OK_ID) {
			if ("".equals(this.funcSubPkgText.getText().trim())){
				String confirmTitle = MiscdpUtil.getIniReader().getValue(CreateDirDialog.class.getSimpleName(),"ConfirmTitle");
				String confirmMsg = MiscdpUtil.getIniReader().getValue(CreateDirDialog.class.getSimpleName(),"CodeCantByNullMsg");
				DialogUtil.showMessage(this.getShell(),confirmTitle, confirmMsg, MESSAGE_TYPE.WARN);
				return;
			}
			
			if ("".equals(this.moduleNameText.getText().trim())){
				String confirmTitle = MiscdpUtil.getIniReader().getValue(CreateDirDialog.class.getSimpleName(),"ConfirmTitle");
				String confirmMsg = MiscdpUtil.getIniReader().getValue(CreateDirDialog.class.getSimpleName(),"NameCantByNullMsg");
				DialogUtil.showMessage(this.getShell(),confirmTitle, confirmMsg, MESSAGE_TYPE.WARN);
				return;
			}
			
			this.insertFuncDir();
			this.funcTree.getFuncTreeViewer().refresh();
			this.close();
			return;
		}
		super.buttonPressed(buttonId);
	}
	private void insertFuncDir(){
		String nextId = String.valueOf(System.currentTimeMillis());
		String newFuncName = this.moduleNameText.getText();
		this.funcId = String.valueOf(nextId);
		this.funcName = newFuncName;
		this.funcSubPkg = this.funcSubPkgText.getText();
	}
	public String getFuncId() {
		return funcId;
	}
	public String getFuncName() {
		return funcName;
	}
	public String getFuncSubPkg() {
		return funcSubPkg;
	}
}