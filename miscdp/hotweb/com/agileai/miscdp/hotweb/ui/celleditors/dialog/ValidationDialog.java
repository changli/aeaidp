package com.agileai.miscdp.hotweb.ui.celleditors.dialog;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.wb.swt.SWTResourceManager;

import com.agileai.miscdp.hotweb.domain.Validation;
import com.agileai.util.StringUtil;
/**
 * 页面校验编辑器提示框
 */
public class ValidationDialog extends Dialog {
	boolean cancelEdit = true;
	@SuppressWarnings("rawtypes")
	private List selectedValidationList = new ArrayList();
	@SuppressWarnings("rawtypes")
	private List inputValidationList = new ArrayList();
	@SuppressWarnings("rawtypes")
	private List midStateValidationList = new ArrayList();
	private CheckboxTableViewer checkboxTableViewer;
	private Label label;
	private static final String DefaultText = "请选择检验类型";
	public ValidationDialog(Shell parentShell) {
		super(parentShell);
	}

	@SuppressWarnings("unchecked")
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		label = new Label(container, SWT.NONE);
		label.setText(DefaultText);
		
		Table table = new Table(container,SWT.MULTI | SWT.BORDER | SWT.CHECK |SWT.FULL_SELECTION);
		table.setHeaderVisible(true);
		table.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				midStateValidationList.clear();
				Object[] objects = checkboxTableViewer.getCheckedElements();
				if (objects != null){
					for (int i=0;i < objects.length;i++){
						Object object = objects[i];	
						midStateValidationList.add(object);
					}
				}
			}
		});
		checkboxTableViewer = new CheckboxTableViewer(table);
		
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		final TableColumn newColumnTableColumn = new TableColumn(table, SWT.NONE);
		newColumnTableColumn.setWidth(154);
		newColumnTableColumn.setText("校验类型");

		final TableColumn newColumnTableColumn_1 = new TableColumn(table, SWT.NONE);
		newColumnTableColumn_1.setWidth(99);
		newColumnTableColumn_1.setText("校验参数");
		checkboxTableViewer.setLabelProvider(new ValidationLabelProvider());
		checkboxTableViewer.setContentProvider(new ArrayContentProvider());
		
		
		final CellEditor[] validationEditor = new CellEditor[table.getColumnCount()]; 
		validationEditor[1] = new TextCellEditor(table);
		
		checkboxTableViewer.setCellEditors(validationEditor);
		checkboxTableViewer.setColumnProperties(Validation.columnProperties); 
		checkboxTableViewer.setCellModifier(new ICellModifier(){
			public boolean canModify(Object element, String property) {
				if ("param0".equals(property)){
					return true;
				}
				return false;
			}
			public Object getValue(Object element, String property) {
				Validation validation = (Validation) element; 
				if ("param0".equals(property)){
					return validation.getParam0();
				}
				return null;
			}
			public void modify(Object element, String property, Object value) {
				TableItem item = (TableItem)element;
				Validation validation = (Validation) item.getData(); 
				if ("param0".equals(property)){
					validation.setParam0(String.valueOf(value));
				}
				checkboxTableViewer.refresh();
			}
		});
		checkboxTableViewer.setInput(inputValidationList);
		checkboxTableViewer.setCheckedElements(selectedValidationList.toArray(new Validation[0]));
		return container;
	}
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}
	protected Point getInitialSize() {
		return new Point(315, 400);
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("表单校验");
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List getValidations() {
		List list = new ArrayList();
		for (int i=0;i < selectedValidationList.size();i++){
			Validation validation = (Validation)selectedValidationList.get(i);
			list.add(validation);
		}
		return list;
	}
	@SuppressWarnings("unchecked")
	public void addValidation(Validation validation) {
		this.selectedValidationList.add(validation);
	}
	@SuppressWarnings("unchecked")
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID) {
			cancelEdit = false;
			this.selectedValidationList.clear();
			for (int i=0;i < midStateValidationList.size();i++){
				Validation validation = (Validation)this.midStateValidationList.get(i);
				String key = validation.getKey();
				if (Validation.Values.LENGTH_LIMIT.equals(key)){
					String param0 = validation.getParam0();
					if (StringUtil.isNullOrEmpty(param0)){
						this.label.setText("注意：长度校验，必须设置长度值！");
						label.setBackground(SWTResourceManager.getColor(SWT.COLOR_RED));
						return;
					}
				}
				this.selectedValidationList.add(validation);
			}
		}
		super.buttonPressed(buttonId);
	}
	@SuppressWarnings("rawtypes")
	public void setInputValidationList(List inputValidationList) {
		this.inputValidationList = inputValidationList;
	}
	public boolean isCancelEdit() {
		return cancelEdit;
	}
	protected Label getLabel() {
		return label;
	}
}
class ValidationLabelProvider extends LabelProvider implements ITableLabelProvider {
	public String getColumnText(Object element, int columnIndex) {
		if (element instanceof Validation) {
			Validation p = (Validation) element;
			if (columnIndex == 0) {
				return p.getValue();
			} else if (columnIndex == 1) {
				return p.getParam0();
			}
		}
		return null;
	}

	public Image getColumnImage(Object element, int columnIndex) {
		return null;
	}
}
