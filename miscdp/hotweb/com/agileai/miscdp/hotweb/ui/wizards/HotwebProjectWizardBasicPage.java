package com.agileai.miscdp.hotweb.ui.wizards;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import com.agileai.common.IniReader;
import com.agileai.miscdp.MiscdpPlugin;
import com.agileai.miscdp.server.HotServerManage;
import com.agileai.miscdp.ui.TextCheckPolicy;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.util.StringUtil;
/**
 * Hotweb工程向导基础配置页
 */
public class HotwebProjectWizardBasicPage extends WizardPage {
	private Text projectLocationText;
	private Text projectNameText;
	private Text mainPackageText;
	
	private String defProjectPath;
	private Text serverAddressText;
	private Text serverPortText;
	private Button javaWebRadioButton;
	private Button portalWebRadioButton;
	private Button bpmWebRadioButton;
	
	private HotwebProjectWizard wizard = null;
	private Composite layoutContainer = null;
	private Text serverUserIdText;
	private Text serverUserPwdText;
	private Button createWSCheckButton;
	private Button hasSysCodeCheckButton;
	private Button dataReportCheckButton;
	
	private Composite createWSComposite;
	private Composite hasSysCodeComposite;
	private Composite dataReportComposite;
	
	public HotwebProjectWizardBasicPage(HotwebProjectWizard wizard) {
		super("");
		IniReader reader = MiscdpUtil.getIniReader(); 
		String title = reader.getValue("ProjectWizard","BasicWizardTitle");
		setTitle(title);
		String desc = reader.getValue("ProjectWizard","BasicWizardDesc");
		setDescription(desc);
		this.wizard = wizard;
		this.defProjectPath = Platform.getLocation().toString();
	}

	public void createControl(Composite parent) {
		this.layoutContainer = new Composite(parent, SWT.NULL);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.verticalSpacing = 3;
		gridLayout.marginWidth = 3;
		gridLayout.numColumns = 1;
		layoutContainer.setLayout(gridLayout);
		setControl(layoutContainer);

		Group basicContainer = new Group(layoutContainer, SWT.NULL);
		basicContainer.setText(Messages.getString("BasicInfo")); 
		GridData gd_basicContainer = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_basicContainer.heightHint = 140;
		basicContainer.setLayoutData(gd_basicContainer);
		final GridLayout gl_basicContainer = new GridLayout();
		gl_basicContainer.verticalSpacing = 3;
		gl_basicContainer.marginWidth = 3;
		gl_basicContainer.numColumns = 2;
		basicContainer.setLayout(gl_basicContainer);
		
		final Label typeLabel = new Label(basicContainer, SWT.NONE);
		typeLabel.setText("应用类型");
		
		Composite composite = new Composite(basicContainer, SWT.NONE);
		composite.setLayout(new GridLayout(3, false));
		composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		javaWebRadioButton = new Button(composite, SWT.RADIO);
		javaWebRadioButton.setSelection(true);
		javaWebRadioButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				serverPortText.setText("6060");
				relayout("JavaWeb");
			}
		});
		javaWebRadioButton.setText(Messages.getString("JavaWebProject")); 
		
		portalWebRadioButton = new Button(composite, SWT.RADIO);
		portalWebRadioButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				serverPortText.setText("8080");
				relayout("PortalWeb");
			}
		});
		portalWebRadioButton.setText(Messages.getString("IntegrateWebProject"));
		
		bpmWebRadioButton = new Button(composite, SWT.RADIO);
		bpmWebRadioButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				serverPortText.setText("7070");
				relayout("BpmWeb");
			}
		});
		bpmWebRadioButton.setText(Messages.getString("bpmWebProject"));
		
		final Label label = new Label(basicContainer, SWT.NONE);
		label.setText("应用名称");

		projectNameText = new Text(basicContainer, SWT.BORDER);
//		projectNameText.addModifyListener(new ModifyListener() {
//			public void modifyText(ModifyEvent e) {
//				basicValidatation();
//			}
//		});
		projectNameText.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				String tempProjectName = projectNameText.getText();
				projectLocationText.setText(defProjectPath+"/"+tempProjectName);
			}
		});
		projectNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		new TextCheckPolicy(projectNameText, TextCheckPolicy.POLICY_FREE_NAME);
		projectNameText.forceFocus();
		
		final Label mainPkgLabel = new Label(basicContainer, SWT.NONE);
		mainPkgLabel.setText("主目录");

		mainPackageText = new Text(basicContainer, SWT.BORDER);
		mainPackageText.setText("com.companyname.projectname");
//		mainPackageText.addModifyListener(new ModifyListener() {
//			public void modifyText(ModifyEvent e) {
//				basicValidatation();
//			}
//		});
		mainPackageText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		

		final Label label_1 = new Label(basicContainer, SWT.NONE);
		label_1.setText("本地路径");

		projectLocationText = new Text(basicContainer, SWT.BORDER);
		projectLocationText.setEditable(false);
		projectLocationText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		projectLocationText.setText(defProjectPath);
		new Label(basicContainer, SWT.NONE);
		
		Composite allComposite = new Composite(basicContainer, SWT.NONE);
		GridLayout gl_allComposite = new GridLayout(3, false);
		gl_allComposite.horizontalSpacing = 0;
		gl_allComposite.marginTop = 5;
		gl_allComposite.marginWidth = 0;
		gl_allComposite.marginHeight = 0;
		allComposite.setLayout(gl_allComposite);
		new Label(basicContainer, SWT.NONE);
		
		createWSComposite = new Composite(allComposite, SWT.NONE);
		GridLayout gl_createWSComposite = new GridLayout(1, false);
		gl_createWSComposite.marginWidth = 0;
		gl_createWSComposite.verticalSpacing = 0;
		gl_createWSComposite.marginHeight = 0;
		gl_createWSComposite.horizontalSpacing = 0;
		createWSComposite.setLayout(gl_createWSComposite);
		
		createWSCheckButton = new Button(createWSComposite, SWT.CHECK);
		createWSCheckButton.setText(Messages.getString("CreateWebService"));
		createWSCheckButton.setSelection(true);
		
		hasSysCodeComposite = new Composite(allComposite, SWT.NONE);
		GridLayout gl_hasSysCodeComposite = new GridLayout(1, false);
		gl_hasSysCodeComposite.marginWidth = 0;
		gl_hasSysCodeComposite.marginHeight = 0;
		gl_hasSysCodeComposite.horizontalSpacing = 0;
		hasSysCodeComposite.setLayout(gl_hasSysCodeComposite);
		
		hasSysCodeCheckButton = new Button(hasSysCodeComposite, SWT.CHECK);
		hasSysCodeCheckButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		hasSysCodeCheckButton.setText(Messages.getString("HasSystemCode"));
		hasSysCodeCheckButton.setVisible(false);
		
		dataReportComposite = new Composite(allComposite, SWT.NONE);
		GridLayout gl_dataReportComposite = new GridLayout(1, false);
		gl_dataReportComposite.marginWidth = 0;
		gl_dataReportComposite.marginHeight = 0;
		gl_dataReportComposite.horizontalSpacing = 0;
		dataReportComposite.setLayout(gl_dataReportComposite);
		
		dataReportCheckButton = new Button(dataReportComposite, SWT.CHECK);
		dataReportCheckButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		dataReportCheckButton.setText(Messages.getString("DataReportProject"));
		dataReportCheckButton.setVisible(false);
		new Label(basicContainer, SWT.NONE);
		dataReportCheckButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (dataReportCheckButton.getSelection()){
					projectNameText.setText("drp");					
				}else{
					projectNameText.setText("bps");
				}
			}
		});
		
		Group serverContainer = new Group(layoutContainer, SWT.NULL);
		serverContainer.setText(Messages.getString("ServerInfo")); 
		serverContainer.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		final GridLayout gl_serverContainer = new GridLayout();
		gl_serverContainer.verticalSpacing = 3;
		gl_serverContainer.marginWidth = 3;
		gl_serverContainer.numColumns = 3;
		serverContainer.setLayout(gl_serverContainer);
		
		Label serverAddressLabel = new Label(serverContainer, SWT.NONE);
		serverAddressLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		serverAddressLabel.setText(Messages.getString("ServerAddress"));
		
		serverAddressText = new Text(serverContainer, SWT.BORDER);
		serverAddressText.setText("localhost"); 
		serverAddressText.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
//		serverAddressText.setEditable(false);
		serverAddressText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new Label(serverContainer, SWT.NONE);
		
		Label serverPortLabel = new Label(serverContainer, SWT.NONE);
		serverPortLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		serverPortLabel.setText(Messages.getString("ServerPort"));
		
		serverPortText = new Text(serverContainer, SWT.BORDER);
		serverPortText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		serverPortText.setText("6060");
		new Label(serverContainer, SWT.NONE);
		
		Label lblNewLabel = new Label(serverContainer, SWT.NONE);
		lblNewLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblNewLabel.setText(Messages.getString("ServerUserId"));
		
		serverUserIdText = new Text(serverContainer, SWT.BORDER);
		serverUserIdText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		serverUserIdText.setText("admin");
		new Label(serverContainer, SWT.NONE);
		
		Label label_2 = new Label(serverContainer, SWT.NONE);
		label_2.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		label_2.setText(Messages.getString("ServerUserPwd"));
		
		serverUserPwdText = new Text(serverContainer, SWT.BORDER | SWT.PASSWORD);
		serverUserPwdText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		serverUserPwdText.setText("admin");
		
		Button btnNewButton = new Button(serverContainer, SWT.NONE);
		btnNewButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				boolean validateResout = basicValidatation();
				if (validateResout){
					try{
						String serverPort = serverPortText.getText();
						String serverAddress = serverAddressText.getText();
						String appName = projectNameText.getText();
						String serverUserId = serverUserIdText.getText();
						String serveruserPwd = serverUserPwdText.getText();
						
						HotServerManage hotServerManage = MiscdpUtil.getHotServerManage(serverAddress, serverPort,serverUserId,serveruserPwd);
						boolean isExist = hotServerManage.isExistContext(appName);
						if (!isExist){
							setPageComplete(true);
							wizard.configPage.setPageComplete(false);
						}else{
							if (bpmWebRadioButton.getSelection()){
								setPageComplete(true);
								wizard.configPage.setPageComplete(true);
							}else{
								setErrorMessage("application named with "+ appName+" is already exist !");
								setPageComplete(false);								
							}
						}
						getWizard().getContainer().updateButtons();
					} catch (Exception ex) {
						setErrorMessage("connect to server failed , please chect it !");
						MiscdpPlugin.getDefault().logError(ex.getLocalizedMessage(), ex);
					}			
				}
			}
		});
		btnNewButton.setText(Messages.getString("TestConnection"));
		this.setPageComplete(false);
	}
	
	public Text getProjectLocationText() {
		return projectLocationText;
	}
	public Text getProjectNameText() {
		return projectNameText;
	}
	public boolean isJavaWebProject(){
		return javaWebRadioButton.getSelection();
	}
	
	public boolean isPortalWebProject(){
		return portalWebRadioButton.getSelection();
	}
	
	public boolean isBpmWebProject(){
		return bpmWebRadioButton.getSelection();
	}
	
	private void relayout(String projectType){
		GridData tempGridData4createWSComposite = (GridData)createWSComposite.getLayoutData();
		GridData tempGridData4hasSysCodeComposite = (GridData)hasSysCodeComposite.getLayoutData();
		GridData tempGridData4dataReportComposite = (GridData)dataReportComposite.getLayoutData();
		
		if ("JavaWeb".equalsIgnoreCase(projectType)){
			createWSCheckButton.setVisible(true);
			hasSysCodeCheckButton.setVisible(false);
			dataReportCheckButton.setVisible(false);
			
			tempGridData4createWSComposite.heightHint = -1;
			tempGridData4createWSComposite.widthHint = -1;
			tempGridData4hasSysCodeComposite.heightHint = 0;
			tempGridData4hasSysCodeComposite.widthHint = 0;
			tempGridData4dataReportComposite.heightHint = 0;
			tempGridData4dataReportComposite.widthHint = 0;
			
			projectNameText.setEditable(true);
			mainPackageText.setEditable(true);
			
			String projectName = projectNameText.getText();
			if (StringUtil.isNotNullNotEmpty(projectName) 
					&& ("bps".equals(projectName) || "drp".equals(projectName))){
				projectNameText.setText("");
			}
			String mainPackage = mainPackageText.getText();
			if (StringUtil.isNotNullNotEmpty(mainPackage) && "com.agileai.bps".equals(mainPackage)){
				mainPackageText.setText("com.companyname.projectname");
			}
		}
		else if ("PortalWeb".equalsIgnoreCase(projectType)){
			createWSCheckButton.setVisible(true);
			hasSysCodeCheckButton.setVisible(true);
			dataReportCheckButton.setVisible(false);
			
			tempGridData4createWSComposite.heightHint = -1;
			tempGridData4createWSComposite.widthHint = -1;
			tempGridData4hasSysCodeComposite.heightHint = -1;
			tempGridData4hasSysCodeComposite.widthHint = -1;
			
			tempGridData4dataReportComposite.heightHint = 0;
			tempGridData4dataReportComposite.widthHint = 0;
			
			projectNameText.setEditable(true);
			mainPackageText.setEditable(true);
			
			String projectName = projectNameText.getText();
			if (StringUtil.isNotNullNotEmpty(projectName) 
					&& ("bps".equals(projectName) || "drp".equals(projectName))){
				projectNameText.setText("");
			}
			String mainPackage = mainPackageText.getText();
			if (StringUtil.isNotNullNotEmpty(mainPackage) && "com.agileai.bps".equals(mainPackage)){
				mainPackageText.setText("com.companyname.projectname");
			}
		}
		else if ("BpmWeb".equalsIgnoreCase(projectType)){
			createWSCheckButton.setVisible(false);
			hasSysCodeCheckButton.setVisible(false);
			dataReportCheckButton.setVisible(true);
			
			tempGridData4createWSComposite.heightHint = 0;
			tempGridData4createWSComposite.widthHint = 0;
			
			tempGridData4hasSysCodeComposite.heightHint = 0;
			tempGridData4hasSysCodeComposite.widthHint = 0;
			
			tempGridData4dataReportComposite.heightHint = -1;
			tempGridData4dataReportComposite.widthHint = -1;
			
			if (dataReportCheckButton.getSelection()){
				projectNameText.setText("drp");
			}else{
				projectNameText.setText("bps");				
			}
			projectNameText.setEditable(false);
			
			mainPackageText.setText("com.agileai.bps");
			mainPackageText.setEditable(false);
		}
		layoutContainer.layout(true, true);
	}
	
	private boolean basicValidatation() {
		String projectName = this.projectNameText.getText();
		if (projectName.length() == 0) {
			updateStatus("project name must be specified !");
			return false;
		}
		if (projectName.length() != projectName.getBytes().length){
			updateStatus("project name must be character !");
			return false;
		}
		IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
		if (project.exists()){
			updateStatus("project is already existed !");
			return false;
		}
		String mainPkgName = this.mainPackageText.getText();
		if (mainPkgName.length() == 0) {
			updateStatus("project main package must be specified !");
			return false;
		}
		if (mainPkgName.length() != mainPkgName.getBytes().length){
			updateStatus("project main package must be character !");
			return false;
		}
		String serverAddress = this.serverAddressText.getText();
		if (serverAddress.length() == 0) {
			updateStatus("server address must be specified !");
			return false;
		}
		String serverPort = this.serverPortText.getText();
		if (serverPort.length() == 0) {
			updateStatus("server port must be specified !");
			return false;
		}
		
		String serverUserId = this.serverUserIdText.getText();
		if (serverUserId.length() == 0) {
			updateStatus("server user name must be specified !");
			return false;
		}
		
		String serverUserPwd = this.serverUserPwdText.getText();
		if (serverUserPwd.length() == 0) {
			updateStatus("server user password must be specified !");
			return false;
		}
		updateStatus(null);
		return true;
	}

	private void updateStatus(String message) {
		setErrorMessage(message);
	}
	public Text getMainPackageText() {
		return mainPackageText;
	}
	public Text getServerAddressText() {
		return serverAddressText;
	}
	public Text getServerPortText() {
		return serverPortText;
	}
	public Button getCreateWSCheckButton() {
		return createWSCheckButton;
	}
	public Text getServerUserIdText() {
		return serverUserIdText;
	}
	public Text getServerUserPwdText() {
		return serverUserPwdText;
	}
	public Button getHasSysCodeCheckButton() {
		return hasSysCodeCheckButton;
	}
	public Composite getHasSysCodeComposite() {
		return hasSysCodeComposite;
	}
	public Button getBpmWebRadioButton() {
		return bpmWebRadioButton;
	}
	public Button getDataReportCheckButton() {
		return dataReportCheckButton;
	}
}