package com.agileai.miscdp.hotweb.ui.actions;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;

import com.agileai.miscdp.DeveloperConst;
import com.agileai.miscdp.MiscdpPlugin;
import com.agileai.miscdp.bpspoxy.FuncModelService;
import com.agileai.miscdp.bpspoxy.FuncModelZip;
import com.agileai.miscdp.hotweb.ConsoleHandler;
import com.agileai.miscdp.hotweb.domain.ProjectConfig;
import com.agileai.miscdp.hotweb.ui.dialogs.FuncModelImportListDialog;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.miscdp.util.ZipHelper;
import com.agileai.util.FileUtil;
import com.agileai.util.IOUtil;

public class ImportFuncModelAction implements IObjectActionDelegate{
    private IWorkbenchPart targetPart;  
    
	public void run(IAction action) {
		ISelection selection = targetPart.getSite().getSelectionProvider().getSelection();
		IStructuredSelection curSelection = (IStructuredSelection)selection;
		IJavaProject javaProject = (IJavaProject)curSelection.getFirstElement();
		try {
			final String projectName = javaProject.getProject().getName();
			Shell shell = targetPart.getSite().getShell();
			FuncModelImportListDialog funcModelImportListDialog = new FuncModelImportListDialog(shell,projectName);
			funcModelImportListDialog.open();
			
			if (funcModelImportListDialog.getReturnCode() == Dialog.OK && funcModelImportListDialog.getSelectedModelCodes().size() > 0){
				processImport(projectName, funcModelImportListDialog.getSelectedModelCodes(),true);
			}
		} catch (Exception e) {
			ConsoleHandler.error(e.getLocalizedMessage());
		}
	}

	public void setActivePart(IAction action, IWorkbenchPart workbenchPart) {
        this.targetPart = workbenchPart;  
	}
	public void selectionChanged(IAction action, ISelection selection) {
		
	}
	
	public static void processImport(final String appName,List<String> selectedModelCodes,boolean onPackageExplorerView){
		InputStream input = null;
		FileOutputStream output = null;
		try {
			ProjectConfig projectConfig = MiscdpUtil.getProjectConfig(appName);
			FuncModelService funcModelService = MiscdpUtil.getFuncModelService(projectConfig);
			FuncModelZip funcModelZip = funcModelService.exportFuncModelZip(selectedModelCodes);
			IProject project = MiscdpUtil.getProject(appName);
			String filePath = project.getLocation().toFile().getParentFile().getAbsolutePath();
			String tempImportFilePath = filePath + File.separator + "toimport" + File.separator + "import.zip";
			File tempExportFile = new File(tempImportFilePath);
			if (!tempExportFile.getParentFile().exists()){
				tempExportFile.getParentFile().mkdirs();
			}
			if (!tempExportFile.exists()){
				tempExportFile.createNewFile();
			}
			input = funcModelZip.getDataHandler().getInputStream();
			output = new FileOutputStream(tempExportFile);
			IOUtil.copy(input, output);
			
			String tempUnzipDirPath = filePath + File.separator + "temp-unzip-dir";
			File tempUnzipDir = new File(tempUnzipDirPath);
			if (tempUnzipDir.exists() && tempUnzipDir.isDirectory()){
				FileUtil.deleteDir(tempUnzipDir);
			}
			tempUnzipDir.mkdir();
			
			ZipHelper zipHelper = new ZipHelper();
			zipHelper.unZip(tempImportFilePath, tempUnzipDirPath);
			File[] unzipedModules = tempUnzipDir.listFiles();
			if (unzipedModules != null && unzipedModules.length > 0){
				File projectFile = project.getLocation().toFile();
				for (int i=0;i < unzipedModules.length;i++){
					File file = unzipedModules[i];
					FileUtil.copyDirectory(file, projectFile);
				}
			}
			if (!onPackageExplorerView){
				IViewPart view = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().findView(DeveloperConst.PackageExplorerViewId);
				if (view != null){
					PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView(DeveloperConst.PackageExplorerViewId);
				}				
			}
			
			Job job = new Job("RefreshJavaProject") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
		            monitor.beginTask(this.getName(), 6000);   
					IProject iProject = MiscdpUtil.getProject(appName);
					try {
						iProject.refreshLocal(IProject.DEPTH_INFINITE, monitor);	
					} catch (Exception e) {
						MiscdpPlugin.getDefault().logError(e.getLocalizedMessage(), e);
					}
		            monitor.done();   
		            return Status.OK_STATUS;   
				}
			};
			job.schedule();  
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			try {
				if (input != null){
					input.close();
				}
				if (output != null){
					output.close();
				}								
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}
}