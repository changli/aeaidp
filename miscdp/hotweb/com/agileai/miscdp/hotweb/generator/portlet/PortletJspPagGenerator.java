﻿package com.agileai.miscdp.hotweb.generator.portlet;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;

import com.agileai.miscdp.MiscdpPlugin;
import com.agileai.miscdp.hotweb.domain.portlet.PortletFuncModel;
import com.agileai.miscdp.hotweb.generator.Generator;

import freemarker.ext.dom.NodeModel;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
/**
 * 最简功能模型JSP代码生成器
 */
public class PortletJspPagGenerator implements Generator{
	private File xmlFile = null;
	
	private String charencoding = "UTF-8";
	private String templateDir = null;
	private String jspPath = null;
	private PortletFuncModel portletFuncModel = null;
	
	public void generate() {
		String fileName = "template";
		try {
			templateDir = FileLocator.toFileURL(Platform.getBundle(MiscdpPlugin.getPluginId()).getResource(fileName)).getFile().toString();
        	Configuration cfg = new Configuration();
        	cfg.setEncoding(Locale.getDefault(), charencoding);
        	cfg.setDirectoryForTemplateLoading(new File(templateDir));
            cfg.setObjectWrapper(new DefaultObjectWrapper());

            String viewTemplateFile = "/portlet/ViewPage.jsp.ftl";
            String viewJspName = jspPath+portletFuncModel.getViewJspName();;
            this.generateViewJsp(cfg, viewTemplateFile, viewJspName);
            
            String editTemplateFile = "/portlet/EditPage.jsp.ftl";
            String editJspName = jspPath+portletFuncModel.getEditJspName();
            this.generateViewJsp(cfg, editTemplateFile, editJspName);
            
            if (portletFuncModel.isGenerateHelp()){
                String helpTemplateFile = "/portlet/HelpPage.jsp.ftl";
                String helpJspName = jspPath + portletFuncModel.getEditJspName().substring(0,portletFuncModel.getEditJspName().length()-8)+"Help.jsp";
                this.generateViewJsp(cfg, helpTemplateFile, helpJspName);
            }
            
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void generateViewJsp(Configuration cfg,String templateFile,String jspName) throws Exception{
    	Template temp = cfg.getTemplate(templateFile,charencoding);
    	temp.setEncoding(charencoding);
        Map root = new HashMap();
        NodeModel nodeModel = freemarker.ext.dom.NodeModel.parse(xmlFile);
        root.put("doc",nodeModel);
        
        File file = new File(jspName);
        File parentFile = file.getParentFile();
        if (!parentFile.exists()){
        	parentFile.mkdirs();
        }
        Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),charencoding));
//        Writer out = new OutputStreamWriter(System.out);
        temp.process(root, out);
        out.flush();
	}
	
	
	public void setPortletFuncModel(PortletFuncModel portletFuncModel) {
		this.portletFuncModel = portletFuncModel;
	}
	
	public void setJspPath(String jspPath) {
		this.jspPath = jspPath;
	}

	public File getXmlFile() {
		return xmlFile;
	}
	public void setXmlFile(File xmlFile) {
		this.xmlFile = xmlFile;
	}
}
