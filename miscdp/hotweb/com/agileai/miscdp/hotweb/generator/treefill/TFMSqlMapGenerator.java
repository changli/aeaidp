﻿package com.agileai.miscdp.hotweb.generator.treefill;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;

import com.agileai.miscdp.DeveloperConst;
import com.agileai.miscdp.MiscdpPlugin;
import com.agileai.miscdp.hotweb.domain.treefill.TreeFillFuncModel;
import com.agileai.miscdp.hotweb.generator.Generator;
import com.agileai.util.XmlUtil;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
/**
 * SQLMap文件代码生成器
 */
public class TFMSqlMapGenerator implements Generator{
	private String sqlMapFile = null;
    private String findRecordsSql = null;
    private TreeFillFuncModel funcModel = null;
    
    private String templateDir = null;
    
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void generateFile() {
		try {
			String fileName = "template";
			try {
				templateDir = FileLocator.toFileURL(Platform.getBundle(MiscdpPlugin.getPluginId()).getResource(fileName)).getFile().toString();
			} catch (IOException e) {
				e.printStackTrace();
			}
			Configuration cfg = new Configuration();
			cfg.setDirectoryForTemplateLoading(new File(templateDir+"/treefill"));
	        cfg.setObjectWrapper(new DefaultObjectWrapper());
	        Template temp = cfg.getTemplate("SqlMap.xml.ftl");
	        Map root = new HashMap();
	        root.put("namespace",funcModel.getSqlNameSpace());
	        FileWriter out = new FileWriter(new File(this.sqlMapFile)); 
//	        Writer out = new OutputStreamWriter(System.out);
	        temp.process(root, out);
	        out.flush();			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
    
	public void generate() {
		try {
			this.generateFile();
			
			Document sqlMapDoc = XmlUtil.readDocument(sqlMapFile,false);
			Element rootElement = sqlMapDoc.getRootElement();
			
	        Element queryRecordsSelectEle = null;
	        String queryRecordsNodePath = "//sqlMap/select[@id='queryPickTreeRecords']";
	        Node queryRecordsNode = sqlMapDoc.selectSingleNode(queryRecordsNodePath);
	        if (queryRecordsNode == null){
	        	queryRecordsSelectEle = rootElement.addElement("select");
	        	queryRecordsSelectEle.addAttribute("id","queryPickTreeRecords");
		        queryRecordsSelectEle.addAttribute("parameterClass",DeveloperConst.PARAMETER_CLASS);
		        queryRecordsSelectEle.addAttribute("resultClass",DeveloperConst.RESULT_CLASS);
		        queryRecordsSelectEle.addText(findRecordsSql);	
	        }
	        else{
	        	queryRecordsNode.detach();
	        	sqlMapDoc.remove(queryRecordsNode);
	        	
//	        	queryRecordsSelectEle = (Element)queryRecordsNode;
//	        	queryRecordsSelectEle.attribute("parameterClass").setValue(DeveloperConst.PARAMETER_CLASS);
//	        	queryRecordsSelectEle.attribute("resultClass").setValue(DeveloperConst.RESULT_CLASS);
//	        	queryRecordsSelectEle.setText(findRecordsSql);
	        	
	        	queryRecordsSelectEle = rootElement.addElement("select");
	        	queryRecordsSelectEle.addAttribute("id","queryPickTreeRecords");
		        queryRecordsSelectEle.addAttribute("parameterClass",DeveloperConst.PARAMETER_CLASS);
		        queryRecordsSelectEle.addAttribute("resultClass",DeveloperConst.RESULT_CLASS);
		        queryRecordsSelectEle.addText(findRecordsSql);	
	        }
        
	        XmlUtil.writeDocument(sqlMapDoc,"UTF-8", sqlMapFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void setSqlMapFile(String sqlMapFile) {
		this.sqlMapFile = sqlMapFile;
	}
	public void setFindRecordsSql(String findRecordsSql) {
		this.findRecordsSql = findRecordsSql;
	}
	public void setFuncModel(TreeFillFuncModel funcModel) {
		this.funcModel = funcModel;
	}
}
