package com.agileai.miscdp.hotweb;

import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;

import com.agileai.miscdp.DeveloperConst;
import com.agileai.miscdp.MiscdpPlugin;

/**
 * Miscdp 属性页
 */
public class HotwebPreferencePage extends PreferencePage implements IWorkbenchPreferencePage{
	private Text projectText;
	protected void performDefaults() {
		super.performDefaults();
		Preferences prefs = InstanceScope.INSTANCE.getNode(MiscdpPlugin.getPluginId());
		prefs.put(DeveloperConst.PROJECT_KEY,"");
		try {
			prefs.flush();
			projectText.setText(prefs.get(DeveloperConst.PROJECT_KEY,""));
		} catch (BackingStoreException e) {
			e.printStackTrace();
		}
	}
	public boolean performOk() {
		boolean result = false;
		Preferences prefs = InstanceScope.INSTANCE.getNode(MiscdpPlugin.getPluginId());
		prefs.put(DeveloperConst.PROJECT_KEY,projectText.getText());
		try {
			prefs.flush();
			result = true;
		} catch (BackingStoreException e) {
			e.printStackTrace();
		}
		return result;
	}

	protected Control createContents(Composite parent) {
		Label label = new Label(parent,SWT.CENTER);
		label.setText("当前工程");
		projectText = new Text(parent,SWT.SINGLE|SWT.BORDER);
		return parent;
	}
	public void init(IWorkbench workbench) {
	}
	public String getProjectName() {
		return projectText.getText();
	}
	public void setProjectName(String projectName) {
		this.projectText.setText(projectName);
	}
}