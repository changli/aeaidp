package com.agileai.miscdp.hotweb.domain.mastersub;

import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.util.StringUtil;

/**
 * 页面表单域
 */
public class SubTableConfig {
	public static final String[] columnProperties = new String[] {"tableName", "editMode","foreignKey","sortField"};
	public static final String[] editModes = new String[] {"entryEditMode","listDetailMode"};
	
	public static final String ENTRY_EDIT_MODE = "entryEditMode";
	public static final String LIST_DETAIL_MODE = "listDetailMode";
	
	public static final String TABLE_NAME = "tableName";
	public static final String EDIT_MODE = "editMode";
	public static final String FOREIGN_KEY = "foreignKey";
	public static final String SORT_FIELD_KEY = "sortField";
	
	private String subTableId = null;
	private String tableName = "";
	private String editMode = "";
	private String foreignKey = "";
	private String queryListSql = "";
	private String primaryKey = "";
	private String tabTitile = "";
	private String pboxJspName ="";
	private String pboxHandlerId ="";
	private String pboxHandlerClass ="";
	private String sortField = "";
	
	public SubTableConfig(){
	}

	public String getEditMode() {
		return editMode;
	}

	public void setEditMode(String editMode) {
		this.editMode = editMode;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getForeignKey() {
		return foreignKey;
	}

	public void setForeignKey(String foreignKey) {
		this.foreignKey = foreignKey;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	
	public String getSubTableId() {
		if (this.subTableId == null){
			this.subTableId = MiscdpUtil.getValidName(this.tableName);
		}
		return subTableId;
	}
	
	public String getPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}

	public String getQueryListSql() {
		return queryListSql;
	}

	public void setQueryListSql(String queryListSql) {
		this.queryListSql = queryListSql;
	}

	public String getTabTitile() {
		return tabTitile;
	}

	public void setTabTitile(String tabTitile) {
		if (!StringUtil.isNullOrEmpty(tabTitile)){
			this.tabTitile = tabTitile;			
		}
	}

	public String getPboxJspName() {
		return pboxJspName;
	}

	public void setPboxJspName(String pboxJspName) {
		this.pboxJspName = pboxJspName;
	}

	public String getPboxHandlerClass() {
		return pboxHandlerClass;
	}

	public void setPboxHandlerClass(String pboxHandlerClass) {
		this.pboxHandlerClass = pboxHandlerClass;
	}

	public String getPboxHandlerId() {
		return pboxHandlerId;
	}

	public void setPboxHandlerId(String pboxHandlerId) {
		this.pboxHandlerId = pboxHandlerId;
	}
}
