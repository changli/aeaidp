package com.agileai.miscdp.hotweb.domain;

import java.util.ArrayList;
import java.util.List;

public class FuncList {
	private String id = null;
	private String name = null;
	private String code = null;
	private List<FuncNode> funcNodes = new ArrayList<FuncNode>();
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public List<FuncNode> getFuncNodes() {
		return funcNodes;
	}
	public void setFuncNodes(List<FuncNode> funcNodes) {
		this.funcNodes = funcNodes;
	}
}