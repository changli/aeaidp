package com.agileai.miscdp.hotweb.domain.treemanage;
/***
 * 树型管理数据表模型
 */
public class TreeManageTableModel {
	private String namespace = null;
	private String tableName = null;
	private String schema = null;
	
	private String queryMaxSortIdSql = null;
	private String queryCurLevelRecordsSql = null;
	private String queryTreeRecordsSql = null;
	private String queryTreeRecordSql = null;
	private String insertTreeRecordSql = null;
	private String updateTreeRecordSql = null;
	private String deleteTreeRecordSql = null;
	private String queryChildRecordsSql = null;
	public String getDeleteTreeRecordSql() {
		return deleteTreeRecordSql;
	}
	public void setDeleteTreeRecordSql(String deleteTreeRecordSql) {
		this.deleteTreeRecordSql = deleteTreeRecordSql;
	}
	public String getInsertTreeRecordSql() {
		return insertTreeRecordSql;
	}
	public void setInsertTreeRecordSql(String insertTreeRecordSql) {
		this.insertTreeRecordSql = insertTreeRecordSql;
	}
	public String getNamespace() {
		return namespace;
	}
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}
	public String getQueryCurLevelRecordsSql() {
		return queryCurLevelRecordsSql;
	}
	public void setQueryCurLevelRecordsSql(String queryCurLevelRecordsSql) {
		this.queryCurLevelRecordsSql = queryCurLevelRecordsSql;
	}
	public String getQueryMaxSortIdSql() {
		return queryMaxSortIdSql;
	}
	public void setQueryMaxSortIdSql(String queryMaxSortIdSql) {
		this.queryMaxSortIdSql = queryMaxSortIdSql;
	}
	public String getQueryTreeRecordSql() {
		return queryTreeRecordSql;
	}
	public void setQueryTreeRecordSql(String queryTreeRecordSql) {
		this.queryTreeRecordSql = queryTreeRecordSql;
	}
	public String getQueryTreeRecordsSql() {
		return queryTreeRecordsSql;
	}
	public void setQueryTreeRecordsSql(String queryTreeRecordsSql) {
		this.queryTreeRecordsSql = queryTreeRecordsSql;
	}
	public String getSchema() {
		return schema;
	}
	public void setSchema(String schema) {
		this.schema = schema;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getUpdateTreeRecordSql() {
		return updateTreeRecordSql;
	}
	public void setUpdateTreeRecordSql(String updateTreeRecordSql) {
		this.updateTreeRecordSql = updateTreeRecordSql;
	}
	public String getQueryChildRecordsSql() {
		return queryChildRecordsSql;
	}
	public void setQueryChildRecordsSql(String queryChildRecordsSql) {
		this.queryChildRecordsSql = queryChildRecordsSql;
	}
}
