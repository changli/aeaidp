package com.agileai.miscdp.hotweb.domain;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import com.agileai.miscdp.DeveloperConst;
/**
 * 功能工程
 */
public class FuncProject implements IEditorInput{
	protected String projectName = "";
	protected String editorId = DeveloperConst.PROJECT_EDITOR_ID;
	
	public boolean exists() {
		return false;
	}
	public ImageDescriptor getImageDescriptor() {
		return null;
	}
	public IPersistableElement getPersistable() {
		return null;
	}

	public String getToolTipText() {
		return this.projectName;
	}
	@SuppressWarnings({"rawtypes" })
	public Object getAdapter(Class adapter) {
		return null;
	}
	public String getName() {
		return this.projectName;
	}

	public String getEditorId() {
		return editorId;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
}
