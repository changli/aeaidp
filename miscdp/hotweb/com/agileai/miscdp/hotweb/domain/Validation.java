﻿package com.agileai.miscdp.hotweb.domain;

import java.util.ArrayList;
import java.util.List;

import com.agileai.common.IniReader;
import com.agileai.domain.KeyNamePair;
import com.agileai.miscdp.util.MiscdpUtil;
/**
 * 页面校验对象
 */
public class Validation extends KeyNamePair{
	public static String[] columnProperties = new String[]{"key","param0"};
	
	public static class Values{
		public static String MUST_NOT_EMPTY= "must_not_empty";
		public static String MUST_BE_UNIQUE= "must_be_unique";
		public static String MUST_BE_INT= "must_be_int";
		public static String MUST_BE_NUM= "must_be_num";
		public static String MUST_BE_CHAR= "must_be_char";
		public static String MUST_BE_DATE= "must_be_date";
		public static String MUST_BE_CHAR8NUMBER= "must_be_char8number";
		public static String MUST_BE_NUM8DOT= "must_be_num8dot";
		public static String MUST_BE_CHAR8NUMBER8DOT= "must_be_char8number8dot";
		public static String MUST_BE_MAIL= "must_be_mail";
		public static String LENGTH_LIMIT= "length_limit";		
	}
	
	private String param0 = null;
	private String param1 = null;

	public String getParam0() {
		return param0;
	}
	public void setParam0(String param0) {
		this.param0 = param0;
	}
	public String getParam1() {
		return param1;
	}
	public void setParam1(String param1) {
		this.param1 = param1;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List prepareInput(){
		List list = new ArrayList();
		IniReader reader = MiscdpUtil.getIniReader();
		List defValueList = reader.getList("Validation");
		for (int i=0;i < defValueList.size();i++){
			KeyNamePair keyNamePair = (KeyNamePair)defValueList.get(i);
			list.add(new Validation().put(keyNamePair.getKey(),keyNamePair.getValue()));					
		}
		return list;
	}
}
