package com.agileai.miscdp.hotweb.domain.treecontent;

import com.agileai.miscdp.hotweb.database.DBManager;
import com.agileai.miscdp.util.MiscdpUtil;

public class ContentTableInfo {
	
	public static final String TABLE_NAME = "tableName";
	public static final String TABLE_MODE = "tableMode";
	public static final String FOREIGN_KEY = "foreignKey";
	public static final String RELATE_TABLE = "relateTable";
	
	public static final String COLUMN_ID = "columnId";
	
	public static final String[] columnProperties = new String[] {"tableName", "tableMode","foreignKey","relateTable"};
	public static final String[] tableModes = new String[] {"One2Many","Many2ManyAndRel"};
	
	public static final class TableMode{
		public static final String One2Many = "One2Many";
		public static final String Many2ManyAndRel = "Many2ManyAndRel";
	}
	private String tabId = null;
	private String tableName = "";
	private String tabName = "";
	private String tableMode = "";
	private String primaryKey = null;
	private String colField = "";
	private String queryListSql = "";
	private String relTableName = "";
	
	public String getColField() {
		return colField;
	}
	public void setColField(String colField) {
		this.colField = colField;
	}
	public String getPrimaryKey(String projectName) {
		if (this.primaryKey == null){
			String pks[] = DBManager.getInstance(projectName).getPrimaryKeys(this.tableName);
			this.primaryKey = pks[0];
		}
		return primaryKey;
	}
	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}
	public String getQueryListSql() {
		return queryListSql;
	}
	public void setQueryListSql(String queryListSql) {
		this.queryListSql = queryListSql;
	}
	public String getRelTableName() {
		return relTableName;
	}
	public void setRelTableName(String relTableName) {
		this.relTableName = relTableName;
	}
	public String getTabId() {
		if (this.tabId == null){
			this.tabId = MiscdpUtil.getValidName(this.tableName);
		}
		return tabId;
	}
	public void setTabId(String tabId) {
		this.tabId = tabId;
	}
	public String getTableMode() {
		return tableMode;
	}
	public void setTableMode(String tableMode) {
		this.tableMode = tableMode;
	}
	public String getTabName() {
		return tabName;
	}
	public void setTabName(String tabName) {
		this.tabName = tabName;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
}