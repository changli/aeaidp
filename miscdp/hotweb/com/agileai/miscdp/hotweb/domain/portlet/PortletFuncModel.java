package com.agileai.miscdp.hotweb.domain.portlet;

import java.io.Reader;
import java.io.StringReader;

import com.agileai.miscdp.DeveloperConst;
import com.agileai.miscdp.hotweb.domain.BaseFuncModel;
import com.agileai.hotweb.model.portlet.PortletFuncModelDocument;

/**
 * Portlet功能模型
 */
public class PortletFuncModel extends BaseFuncModel {
	
	private String portletClass = "";
	private String portletName = "";
	private String keywords = "";
	private String portletTitle = "";
	private String viewJspName = "";
	private String editJspName = "";
	private boolean generateHelp = false;
	private boolean ajaxLoadData = false;
	
	public PortletFuncModel(){
		this.editorId = DeveloperConst.PORTLET_EDITOR_ID;
		this.template = "Default";
	}

	public void buildFuncModel(String funcDefine) {
		PortletFuncModel portletFuncModel = this;
		try {
			Reader reader = new StringReader(funcDefine);
			PortletFuncModelDocument funcModelDocument = PortletFuncModelDocument.Factory.parse(reader);
			PortletFuncModelDocument.PortletFuncModel.BaseInfo baseInfo = funcModelDocument.getPortletFuncModel().getBaseInfo();

			portletFuncModel.setPortletTitle(this.getFuncName());
			
			portletFuncModel.setPortletClass(baseInfo.getPortletClass());
			portletFuncModel.setPortletName(baseInfo.getPortletName());
			portletFuncModel.setKeywords(baseInfo.getKeywords());
			portletFuncModel.setViewJspName(baseInfo.getViewJspName());
			portletFuncModel.setEditJspName(baseInfo.getEditJspName());
			portletFuncModel.setGenerateHelp(baseInfo.getGenerateHelp());
			portletFuncModel.setAjaxLoadData(baseInfo.getAjaxLoadData());
			
			portletFuncModel.setTemplate(baseInfo.getTemplate());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getPortletClass() {
		return portletClass;
	}

	public void setPortletClass(String portletClass) {
		this.portletClass = portletClass;
	}

	public String getPortletName() {
		return portletName;
	}

	public void setPortletName(String portletName) {
		this.portletName = portletName;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getPortletTitle() {
		return portletTitle;
	}

	public void setPortletTitle(String portletTitle) {
		this.portletTitle = portletTitle;
	}

	public String getViewJspName() {
		return viewJspName;
	}

	public void setViewJspName(String viewJspName) {
		this.viewJspName = viewJspName;
	}

	public String getEditJspName() {
		return editJspName;
	}

	public void setEditJspName(String editJspName) {
		this.editJspName = editJspName;
	}

	public boolean isGenerateHelp() {
		return generateHelp;
	}

	public void setGenerateHelp(boolean generateHelp) {
		this.generateHelp = generateHelp;
	}

	public boolean isAjaxLoadData() {
		return ajaxLoadData;
	}

	public void setAjaxLoadData(boolean ajaxLoadData) {
		this.ajaxLoadData = ajaxLoadData;
	}
}
