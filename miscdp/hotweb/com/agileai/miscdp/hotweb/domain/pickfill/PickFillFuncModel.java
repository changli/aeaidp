package com.agileai.miscdp.hotweb.domain.pickfill;

import java.io.Reader;
import java.io.StringReader;

import com.agileai.miscdp.DeveloperConst;
import com.agileai.miscdp.hotweb.domain.standard.StandardFuncModel;
import com.agileai.hotweb.model.FormObjectDocument.FormObject;
import com.agileai.hotweb.model.pickfill.PickFillFuncModelDocument;
/**
 * 查询功能模型
 */
public class PickFillFuncModel extends StandardFuncModel{
	private boolean isMultiSelect = false;
	private String sqlMapFile = "";
	private String sqlNameSpace = "";
	private String codeField = "";
	private String nameField = "";
	
	public PickFillFuncModel(){
		this.editorId = DeveloperConst.PICKFILL_EDITOR_ID;
	}
	public void buildFuncModel(String funcDefine){
		PickFillFuncModel pickFillFuncModel = this;
		try {
			Reader reader = new StringReader(funcDefine);
			PickFillFuncModelDocument funcModelDocument = PickFillFuncModelDocument.Factory.parse(reader);
			
			PickFillFuncModelDocument.PickFillFuncModel.BaseInfo baseInfo = funcModelDocument.getPickFillFuncModel().getBaseInfo();
			pickFillFuncModel.setListHandlerId(baseInfo.getHandler().getListHandlerId());
			pickFillFuncModel.setListHandlerClass(baseInfo.getHandler().getListHandlerClass());
			
			pickFillFuncModel.setServiceId(baseInfo.getService().getServiceId());
			pickFillFuncModel.setImplClassName(baseInfo.getService().getImplClassName());
			pickFillFuncModel.setInterfaceName(baseInfo.getService().getInterfaceName());
			pickFillFuncModel.setListJspName(baseInfo.getListJspName());
			pickFillFuncModel.setListTitle(baseInfo.getListTitle());
			pickFillFuncModel.setCodeField(baseInfo.getCodeField());
			pickFillFuncModel.setNameField(baseInfo.getNameField());
			pickFillFuncModel.setIsMultiSelect(baseInfo.getIsMultiSelect());
			pickFillFuncModel.setTemplate(baseInfo.getTemplate());
			pickFillFuncModel.setListSql(baseInfo.getQueryListSql());
			String sqlMapPath = baseInfo.getSqlResource().getSqlMapPath();
			pickFillFuncModel.setSqlMapFile(sqlMapPath);
            
			PickFillFuncModelDocument.PickFillFuncModel.ListView listView = funcModelDocument.getPickFillFuncModel().getListView();
			String rsIdColumnTemp  = listView.getListTableArea().getRow().getRsIdColumn();
			String[] rsIdColumnArray = rsIdColumnTemp.split(",");
			for (int i=0;i < rsIdColumnArray.length;i++){
				pickFillFuncModel.getRsIdColumns().add(rsIdColumnArray[i]);				
			}
			
			FormObject[] formObjects = listView.getParameterArea().getFormObjectArray();
			processPageParamters(formObjects, pageParameters);
			
			com.agileai.hotweb.model.ListTableType listTableType = listView.getListTableArea();
			pickFillFuncModel.setExportCsv(listTableType.getExportCsv());
			pickFillFuncModel.setExportXls(listTableType.getExportXls());
			pickFillFuncModel.setSortAble(listTableType.getSortAble());
			pickFillFuncModel.setPagination(listTableType.getPagination());

			com.agileai.hotweb.model.ListTableType.Row.Column[] columns = listTableType.getRow().getColumnArray();
			processListTableColumns(columns, listTableColumns);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public String getSqlMapFile() {
		return sqlMapFile;
	}
	public void setSqlMapFile(String sqlMapFile) {
		this.sqlMapFile = sqlMapFile;
	}
	public String getSqlNameSpace() {
		return sqlNameSpace;
	}
	public void setSqlNameSpace(String sqlNameSpace) {
		this.sqlNameSpace = sqlNameSpace;
	}
	public String getCodeField() {
		return codeField;
	}
	public String getNameField() {
		return nameField;
	}
	public boolean getIsMultiSelect() {
		return isMultiSelect;
	}
	public void setIsMultiSelect(boolean isMultiSelect) {
		this.isMultiSelect = isMultiSelect;
	}
	public void setCodeField(String codeField) {
		this.codeField = codeField;
	}
	public void setNameField(String nameField) {
		this.nameField = nameField;
	}
}
