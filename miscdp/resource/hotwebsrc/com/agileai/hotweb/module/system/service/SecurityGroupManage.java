package com.agileai.hotweb.module.system.service;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeAndContentManage;

public interface SecurityGroupManage
        extends TreeAndContentManage {
	
	void delGroupTreeRelation(DataParam param);
	public void insertGroupRoleRelation(DataParam param);
	public List<DataRow> queryRelationRecords (String currentId);
	public List<DataRow>  findRoleRecords(String roleId);
	public List<DataRow> findGroupRoleRelRecords(String roleId);
	public List<DataRow> findRoleTreeRecords(DataParam param);
	public List<DataRow>  queryChildRecords(String rolId);
}
