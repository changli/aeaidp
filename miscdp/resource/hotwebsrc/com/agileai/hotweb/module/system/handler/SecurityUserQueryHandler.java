package com.agileai.hotweb.module.system.handler;

import java.util.List;

import com.agileai.common.KeyGenerator;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.PickFillModelHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.module.system.service.SecurityUserManage;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;


public class SecurityUserQueryHandler
        extends PickFillModelHandler {
    public SecurityUserQueryHandler() {
        super();
        this.serviceId = buildServiceId(SecurityUserManage.class);
    }

    protected void processPageAttributes(DataParam param) {
    	 initMappingItem("USER_STATE",
                 FormSelectFactory.create("SYS_VALID_TYPE")
                                  .getContent());
    	
    }

    protected void initParameters(DataParam param) {
    }
    
    public ViewRenderer prepareDisplay(DataParam param){
    	SecurityUserManage securityUserManage = (SecurityUserManage)this.lookupService(SecurityUserManage.class);
		mergeParam(param);
		initParameters(param);
		this.setAttributes(param);
		String rgId = param.get("RG_ID");
		List<DataRow> rsList = securityUserManage.findPickFillRecords(rgId);
		this.setRsList(rsList);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}	
    
    public ViewRenderer doSaveUserAction(DataParam param){
    	SecurityUserManage securityUserManage = (SecurityUserManage)this.lookupService(SecurityUserManage.class);
    	String rspText = SUCCESS;
    	String userIds = param.get("USER_ID");
    	String rgId = param.getString("RG_ID");
    	String[] userArray = userIds.split(",");
    	for(int i = 0; i < userArray.length; i++){
    		String userId = userArray[i];
    		String urgId = KeyGenerator.instance().genKey();
    		DataParam tempParam=new DataParam();
    		tempParam.put("URG_ID",urgId);
    		tempParam.put("RG_ID",rgId );
    		tempParam.put("USER_ID",userId);
    		securityUserManage.insertUserActualRoleRecords(tempParam);
    	}	
    	return new AjaxRenderer(rspText);
    }
  
}
