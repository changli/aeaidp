<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>系统日志查询</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script type="text/javascript">
var detailBox;
function showDetailBox(){
	if (!isSelectedRow()){
		writeErrorMsg("请先选中一条记录！");
		return;
	}
	if (!detailBox){
		detailBox = new PopupBox('detailBox','明细信息查看',{size:'big',height:'400px',top:'30px'});
	}
	var url = "<%=pageBean.getHandlerURL()%>&actionType=viewDetail&ID="+$('#ID').val();
	detailBox.sendRequest(url);
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ToolBar__">
<table class="toolTable" border="0" cellpadding="0" cellspacing="1">
<tr>
	<td onMouseOver="onMover(this);" onMouseOut="onMout(this);" class="bartdx" align="center" onClick="showDetailBox()"><input id="viewDetail" value="&nbsp;" type="button" class="detailImgBtn" title="查看" />查看</td>
	<td onMouseOver="onMover(this);" onMouseOut="onMout(this);" class="bartdx" align="center" onClick="doQuery();"><input id="refreshImgBtn" value="&nbsp;" type="button" class="refreshImgBtn" title="刷新" />刷新</td>
</tr>
</table>
</div>
<div id="__ParamBar__">
<table class="queryTable"><tr><td>
&nbsp;起止日期<input id="sdate" label="起止日期" name="sdate" type="text" value="<%=pageBean.inputValue("sdate")%>" size="24" class="text" />

&nbsp;-<input id="edate" label="-" name="edate" type="text" value="<%=pageBean.inputValue("edate")%>" size="24" class="text" />

&nbsp;<input type="button" name="button" id="button" value="查询" class="formbutton" onclick="doQuery()" />
</td></tr></table>
</div>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" csvFileName="系统日志查询.csv"
retrieveRowsCallback="process" xlsFileName="系统日志查询.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |export|extend|status"
width="100%" rowsDisplayed="15"
listWidth="100%" 
height="390px"
>
<ec:row styleClass="odd" ondblclick="showDetailBox()" oncontextmenu="selectRow(this,{ID:'${row.ID}'});refreshConextmenu()" onclick="selectRow(this,{ID:'${row.ID}'})">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="100" property="USER_NAME" title="访问人员"   />
	<ec:column width="100" property="USER_ID" title="人员帐号"   />
	<ec:column width="100" property="OPER_TIME" title="时间"   />
	<ec:column width="100" property="IP_ADDTRESS" title="IP地址"   />
	<ec:column width="100" property="FUNC_NAME" title="功能名称"   />
	<ec:column width="100" property="ACTION_TYPE" title="操作类型"   />
</ec:row>
</ec:table>
<input type="hidden" name="ID" id="ID" value="" />
<input type="hidden" name="actionType" id="actionType" />
<script language="JavaScript">
setRsIdTag('ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
