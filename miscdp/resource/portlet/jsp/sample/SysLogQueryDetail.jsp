<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>系统日志查询</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
</head>
<body>
<script language="javascript">
wrTitle('系统日志查询');
</script>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>时间</th>
	<td><input id="OPER_TIME" label="时间" name="OPER_TIME" type="text" value="<%=pageBean.inputValue("OPER_TIME")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>IP地址</th>
	<td><input id="IP_ADDTRESS" label="IP地址" name="IP_ADDTRESS" type="text" value="<%=pageBean.inputValue("IP_ADDTRESS")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>人员帐号</th>
	<td><input id="USER_ID" label="人员帐号" name="USER_ID" type="text" value="<%=pageBean.inputValue("USER_ID")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>访问人员</th>
	<td><input id="USER_NAME" label="访问人员" name="USER_NAME" type="text" value="<%=pageBean.inputValue("USER_NAME")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>功能名称</th>
	<td><input id="FUNC_NAME" label="功能名称" name="FUNC_NAME" type="text" value="<%=pageBean.inputValue("FUNC_NAME")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>操作类型</th>
	<td><input id="ACTION_TYPE" label="操作类型" name="ACTION_TYPE" type="text" value="<%=pageBean.inputValue("ACTION_TYPE")%>" size="24" class="text" />
</td>
</tr>
</table>
<table width="100%" border="0">
  <tr>
    <td align="center"><input type="button" name="button" id="button" value="关闭窗口" class="formbutton" onClick="parent.PopupBox.closeCurrent()"></td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value="" />
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>" />
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
