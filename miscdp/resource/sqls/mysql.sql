/*
Navicat MySQL Data Transfer

Source Server         : local_mysql
Source Server Version : 50717
Source Host           : 127.0.0.1:3306
Source Database       : hotweb

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2018-01-15 18:56:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for security_group
-- ----------------------------
DROP TABLE IF EXISTS `security_group`;
CREATE TABLE `security_group` (
  `GRP_ID` varchar(36) NOT NULL,
  `GRP_CODE` varchar(32) DEFAULT NULL,
  `GRP_NAME` varchar(32) DEFAULT NULL,
  `GRP_PID` varchar(36) DEFAULT NULL,
  `GRP_DESC` varchar(128) DEFAULT NULL,
  `GRP_STATE` varchar(1) DEFAULT NULL,
  `GRP_SORT` int(11) DEFAULT NULL,
  `GRP_TYPE` varchar(32) DEFAULT NULL,
  `GRP_RANK` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`GRP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_group
-- ----------------------------
INSERT INTO `security_group` VALUES ('00000000-0000-0000-00000000000000000', 'Root', '公司集团', null, null, '1', '0', 'company', '1');
INSERT INTO `security_group` VALUES ('19690068-D8B4-4B97-8E4D-5D837A8B9AD5', 'C0006', '行政部', 'EEDB6308-A8A9-40CF-A078-4606B6EF4622', '', '1', '5', 'department', '1');
INSERT INTO `security_group` VALUES ('1C023241-9315-4764-A7F1-7E90537CD7A7', 'A0001', '技术部', '00000000-0000-0000-00000000000000000', '', '1', '3', 'department', '1');
INSERT INTO `security_group` VALUES ('3A26DB2F-ECA1-4C89-A971-94F501EFCD41', 'D0001', '上海分公司', '00000000-0000-0000-00000000000000000', '', '1', '6', 'company', '1');
INSERT INTO `security_group` VALUES ('6FC0686E-39D0-4DCE-AF54-A80738EAA28B', 'C0005', '销售部', 'EEDB6308-A8A9-40CF-A078-4606B6EF4622', '', '1', '4', 'department', '1');
INSERT INTO `security_group` VALUES ('8BF2F65A-6765-46CC-A853-9B75304FC7FD', '002.01', '技术部', 'EEDB6308-A8A9-40CF-A078-4606B6EF4622', '', '1', '2', 'department', '1');
INSERT INTO `security_group` VALUES ('B17F9D2B-837F-4247-8A34-19E71BFC6DB4', 'D0006', '销售部', '3A26DB2F-ECA1-4C89-A971-94F501EFCD41', '', '1', '4', 'department', '1');
INSERT INTO `security_group` VALUES ('B6F11BD3-DE55-4F14-A400-540B9E4F45F3', 'A0002', '信息部', '00000000-0000-0000-00000000000000000', '', '1', '2', 'department', '1');
INSERT INTO `security_group` VALUES ('DFA63AC0-6D21-443F-B26C-97118A1808B8', 'D0002', '技术部', '3A26DB2F-ECA1-4C89-A971-94F501EFCD41', '', '1', '1', 'department', '2');
INSERT INTO `security_group` VALUES ('E068CD3C-E676-4601-B5B8-5CB9F04C5D37', 'C0002', '信息部', 'EEDB6308-A8A9-40CF-A078-4606B6EF4622', '', '1', '3', 'department', '1');
INSERT INTO `security_group` VALUES ('E13A2B44-8965-4023-9394-71CA9BF4B899', 'D0005', '信息部', '3A26DB2F-ECA1-4C89-A971-94F501EFCD41', '', '1', '3', 'department', '1');
INSERT INTO `security_group` VALUES ('E49B519B-7DC3-4104-BEBF-CC0B05833A49', 'A0003', '行政部', '00000000-0000-0000-00000000000000000', '', '1', '4', 'department', '1');
INSERT INTO `security_group` VALUES ('EEDB6308-A8A9-40CF-A078-4606B6EF4622', 'B0001', '北京分公司', '00000000-0000-0000-00000000000000000', '', '1', '5', 'company', '1');
INSERT INTO `security_group` VALUES ('FE8A88A0-5CFB-42C7-B398-E4AE74A8E39D', 'D0003', '行政部', '3A26DB2F-ECA1-4C89-A971-94F501EFCD41', '', null, '2', 'department', '1');

-- ----------------------------
-- Table structure for security_group_auth
-- ----------------------------
DROP TABLE IF EXISTS `security_group_auth`;
CREATE TABLE `security_group_auth` (
  `GRP_AUTH_ID` varchar(36) NOT NULL,
  `GRP_ID` varchar(36) DEFAULT NULL,
  `RES_TYPE` varchar(32) DEFAULT NULL,
  `RES_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`GRP_AUTH_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_group_auth
-- ----------------------------
INSERT INTO `security_group_auth` VALUES ('4C20EDAC-0A0B-45BB-8B71-B557A94E7429', 'B6F11BD3-DE55-4F14-A400-540B9E4F45F3', 'Navigater', '02');

-- ----------------------------
-- Table structure for security_rg_auth
-- ----------------------------
DROP TABLE IF EXISTS `security_rg_auth`;
CREATE TABLE `security_rg_auth` (
  `RG_AUTH_ID` char(36) NOT NULL,
  `RG_ID` char(36) DEFAULT NULL,
  `RES_TYPE` varchar(32) DEFAULT NULL,
  `RES_ID` char(36) DEFAULT NULL,
  PRIMARY KEY (`RG_AUTH_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_rg_auth
-- ----------------------------
INSERT INTO `security_rg_auth` VALUES ('C922C243-8C3C-479B-9E84-792EDAEDEB43', 'A0CFD5D1-6D43-4A74-8A92-1B06F6AB95D3', 'Menu', 'E31A6BD9-8842-4F16-A2B2-929A2E831173');
INSERT INTO `security_rg_auth` VALUES ('DAC29F7B-3498-4227-940A-70F3E930C00B', 'A0CFD5D1-6D43-4A74-8A92-1B06F6AB95D3', 'Menu', '8F1E1FC2-423E-4216-BBEF-916DEE23DEDC');
INSERT INTO `security_rg_auth` VALUES ('E86369BD-6B18-4E62-947A-56C1C265F232', 'A0CFD5D1-6D43-4A74-8A92-1B06F6AB95D3', 'Menu', '00000000-0000-0000-00000000000000000');

-- ----------------------------
-- Table structure for security_role
-- ----------------------------
DROP TABLE IF EXISTS `security_role`;
CREATE TABLE `security_role` (
  `ROLE_ID` varchar(36) NOT NULL,
  `ROLE_CODE` varchar(32) DEFAULT NULL,
  `ROLE_NAME` varchar(32) DEFAULT NULL,
  `ROLE_TYPE` varchar(32) DEFAULT NULL,
  `ROLE_PID` varchar(36) DEFAULT NULL,
  `ROLE_DESC` varchar(128) DEFAULT NULL,
  `ROLE_STATE` varchar(32) DEFAULT NULL,
  `ROLE_SORT` int(11) DEFAULT NULL,
  PRIMARY KEY (`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_role
-- ----------------------------
INSERT INTO `security_role` VALUES ('00000000-0000-0000-00000000000000000', 'System', '系统角色', 'menu', null, null, '1', null);
INSERT INTO `security_role` VALUES ('8752C789-44BD-4941-AEF1-B9CC8B5C3CC6', 'ITManager', 'IT主管', 'part', '00000000-0000-0000-00000000000000000', '', '1', '3');

-- ----------------------------
-- Table structure for security_role_auth
-- ----------------------------
DROP TABLE IF EXISTS `security_role_auth`;
CREATE TABLE `security_role_auth` (
  `ROLE_AUTH_ID` varchar(36) NOT NULL,
  `ROLE_ID` varchar(36) DEFAULT NULL,
  `RES_TYPE` varchar(32) DEFAULT NULL,
  `RES_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`ROLE_AUTH_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_role_auth
-- ----------------------------
INSERT INTO `security_role_auth` VALUES ('586BF6FA-4066-4C65-9352-C0A17B6E2170', '8752C789-44BD-4941-AEF1-B9CC8B5C3CC6', 'Menu', '00000000-0000-0000-00000000000000000');

-- ----------------------------
-- Table structure for security_role_group_rel
-- ----------------------------
DROP TABLE IF EXISTS `security_role_group_rel`;
CREATE TABLE `security_role_group_rel` (
  `RG_ID` char(36) NOT NULL,
  `GRP_ID` varchar(36) NOT NULL,
  `ROLE_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`RG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_role_group_rel
-- ----------------------------
INSERT INTO `security_role_group_rel` VALUES ('A0CFD5D1-6D43-4A74-8A92-1B06F6AB95D3', '00000000-0000-0000-00000000000000000', '8752C789-44BD-4941-AEF1-B9CC8B5C3CC6');
INSERT INTO `security_role_group_rel` VALUES ('F49AD3BA-A714-44E7-9B97-4B4F595C20F1', 'B6F11BD3-DE55-4F14-A400-540B9E4F45F3', '8752C789-44BD-4941-AEF1-B9CC8B5C3CC6');

-- ----------------------------
-- Table structure for security_user
-- ----------------------------
DROP TABLE IF EXISTS `security_user`;
CREATE TABLE `security_user` (
  `USER_ID` varchar(36) NOT NULL,
  `USER_CODE` varchar(32) DEFAULT NULL,
  `USER_NAME` varchar(32) DEFAULT NULL,
  `USER_PWD` varchar(32) DEFAULT NULL,
  `USER_SEX` varchar(1) DEFAULT NULL,
  `USER_DESC` varchar(128) DEFAULT NULL,
  `USER_STATE` varchar(32) DEFAULT NULL,
  `USER_SORT` int(11) DEFAULT NULL,
  `USER_MAIL` varchar(64) DEFAULT NULL,
  `USER_PHONE` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_user
-- ----------------------------
INSERT INTO `security_user` VALUES ('4820B253-DF9A-476F-B1B1-1D21754256CF', 'user', '默认用户', '4124BC0A9335C27F086F24BA207A4912', 'M', '默认内置用户，勿删！！', '1', '2', '', '');
INSERT INTO `security_user` VALUES ('7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24', 'admin', '管理员', '21232F297A57A5A743894A0E4A801FC3', 'M', '内置账户，勿删！！', '1', '1', null, null);

-- ----------------------------
-- Table structure for security_user_auth
-- ----------------------------
DROP TABLE IF EXISTS `security_user_auth`;
CREATE TABLE `security_user_auth` (
  `USER_AUTH_ID` varchar(36) NOT NULL,
  `USER_ID` varchar(36) DEFAULT NULL,
  `RES_TYPE` varchar(32) DEFAULT NULL,
  `RES_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`USER_AUTH_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_user_auth
-- ----------------------------
INSERT INTO `security_user_auth` VALUES ('1279D0FE-B366-4D7E-9C99-B6B3424387F3', 'B64DCBC3-F28A-4DC8-92F1-1FB10367CACC', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_user_auth` VALUES ('4B523CC8-775A-49A1-BF87-28A7FD05B81F', '7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24', 'Menu', '271576B9-3F0F-49B6-8505-3999FDB3E795');
INSERT INTO `security_user_auth` VALUES ('5194F17D-B001-44EE-9FAB-21F836CB49A8', 'B64DCBC3-F28A-4DC8-92F1-1FB10367CACC', 'Handler', '494DF09B-7573-4CCA-85C1-97F4DC58C86B');
INSERT INTO `security_user_auth` VALUES ('63253005-BECA-4C51-A764-C36342DA009F', 'B64DCBC3-F28A-4DC8-92F1-1FB10367CACC', 'Menu', '00000000-0000-0000-00000000000000001');
INSERT INTO `security_user_auth` VALUES ('7CDFBA30-A40B-4958-820F-37171A5782C5', '7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24', 'Menu', '11CB05AE-204D-4207-9AF6-ABB7084F0C25');
INSERT INTO `security_user_auth` VALUES ('81882EE9-6E6C-4BEF-9F41-55D135784C0C', '7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24', 'Menu', 'D3FAC05D-12F3-4576-ABC6-DAD037D1F4F9');
INSERT INTO `security_user_auth` VALUES ('97F07CAD-F046-48FB-B03B-5A0AA049FBD5', 'B64DCBC3-F28A-4DC8-92F1-1FB10367CACC', 'Menu', '67BA273A-DD31-48D0-B78C-1D60D5316074');
INSERT INTO `security_user_auth` VALUES ('CB82AB8B-2066-4647-B8B3-2E2BFC0394CF', '7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24', 'Menu', '590EE84E-5EC9-4197-A37C-CE67E191DC56');
INSERT INTO `security_user_auth` VALUES ('D76FA80F-51AB-4A5B-9594-6DAE53A3C1E3', 'B64DCBC3-F28A-4DC8-92F1-1FB10367CACC', 'Operation', '3EC41FB8-ED98-41F3-B86D-F7473AEDD002');
INSERT INTO `security_user_auth` VALUES ('E3BB835C-FE79-4B8B-B412-91A0B7D92CCA', '7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_user_auth` VALUES ('ECCA6C17-0077-4FDA-94A7-D4637AD5D8AC', '7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24', 'Menu', 'B7836820-C2E2-4A87-8497-A1B22A0EA9E0');

-- ----------------------------
-- Table structure for security_user_group_rel
-- ----------------------------
DROP TABLE IF EXISTS `security_user_group_rel`;
CREATE TABLE `security_user_group_rel` (
  `GU_ID` char(36) NOT NULL,
  `GRP_ID` varchar(36) DEFAULT NULL,
  `USER_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`GU_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_user_group_rel
-- ----------------------------
INSERT INTO `security_user_group_rel` VALUES ('0665161F-029D-4329-9EE7-3F035DC4CB34', '00000000-0000-0000-00000000000000000', '7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24');
INSERT INTO `security_user_group_rel` VALUES ('096126DD-39AB-44CF-A09E-671AD00BF5F8', 'B6F11BD3-DE55-4F14-A400-540B9E4F45F3', '9A0A9DE7-608A-4B2B-B1C7-F6C11FD1A94E');
INSERT INTO `security_user_group_rel` VALUES ('09DF8B4D-FC87-48F0-B799-EEDA383DB808', 'B6F11BD3-DE55-4F14-A400-540B9E4F45F3', 'B64DCBC3-F28A-4DC8-92F1-1FB10367CACC');

-- ----------------------------
-- Table structure for security_user_rg_rel
-- ----------------------------
DROP TABLE IF EXISTS `security_user_rg_rel`;
CREATE TABLE `security_user_rg_rel` (
  `URG_ID` varchar(36) NOT NULL,
  `USER_ID` varchar(36) DEFAULT NULL,
  `RG_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`URG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_user_rg_rel
-- ----------------------------

-- ----------------------------
-- Table structure for sys_codelist
-- ----------------------------
DROP TABLE IF EXISTS `sys_codelist`;
CREATE TABLE `sys_codelist` (
  `TYPE_ID` varchar(32) NOT NULL,
  `CODE_ID` varchar(64) NOT NULL,
  `CODE_NAME` varchar(64) DEFAULT NULL,
  `CODE_DESC` varchar(512) DEFAULT NULL,
  `CODE_SORT` int(11) DEFAULT NULL,
  `CODE_FLAG` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`TYPE_ID`,`CODE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_codelist
-- ----------------------------
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'Bottom', 'BottomHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'Building', 'BuildingHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'Homepage', 'HomepageHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'Logo', 'LogoHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'MainWin', 'MainWinHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'MenuTree', 'MenuTreeHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'Navigater', 'NavigaterHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('BOOL_DEFINE', 'N', '否', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('BOOL_DEFINE', 'Y', '是', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('CODE_TYPE_GROUP', 'app_code_define', '应用编码', 'null', '1', '1');
INSERT INTO `sys_codelist` VALUES ('CODE_TYPE_GROUP', 'sys_code_define', '系统编码', '系统编码123a1', '3', '1');
INSERT INTO `sys_codelist` VALUES ('FUNCTION_TYPE', 'funcmenu', '功能菜单', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('FUNCTION_TYPE', 'funcnode', '功能节点', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('HANDLER_TYPE', 'MAIN', '主控制器', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('HANDLER_TYPE', 'OTHER', '其他控制器', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('INFO_TYPE', 'menu', '目录', null, '1', '1');
INSERT INTO `sys_codelist` VALUES ('INFO_TYPE', 'part', '角色', null, '2', '1');
INSERT INTO `sys_codelist` VALUES ('MENUTREE_CASCADE', '0', '关闭', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('MENUTREE_CASCADE', '1', '展开', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('OPER_CTR_TYPE', 'disableMode', '不能操作', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('OPER_CTR_TYPE', 'hiddenMode', '隐藏按钮', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('ORGNAZITION_RANK', '1', '一级', null, '1', '1');
INSERT INTO `sys_codelist` VALUES ('ORGNAZITION_RANK', '2', '二级', null, '2', '1');
INSERT INTO `sys_codelist` VALUES ('ORGNAZITION_RANK', '3', '三级', null, '3', '1');
INSERT INTO `sys_codelist` VALUES ('ORGNAZITION_TYPE', 'company', '公司', null, '1', '1');
INSERT INTO `sys_codelist` VALUES ('ORGNAZITION_TYPE', 'department', '部门', null, '2', '1');
INSERT INTO `sys_codelist` VALUES ('POSITION_TYPE', 'dummy_postion', '虚拟岗位', 'null', '1', '1');
INSERT INTO `sys_codelist` VALUES ('POSITION_TYPE', 'real_postion', '实际岗位', 'null', '1', '1');
INSERT INTO `sys_codelist` VALUES ('RES_TYPE', 'IMAGE', '图片文件', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('RES_TYPE', 'ISO', '镜像文件', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('RES_TYPE', 'VIDEO', '视频文件', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('SYS_VALID_TYPE', '0', '无效', 'null', '2', '1');
INSERT INTO `sys_codelist` VALUES ('SYS_VALID_TYPE', '1', '有效', 'null', '1', '1');
INSERT INTO `sys_codelist` VALUES ('UNIT_TYPE', 'dept', '部门', '', '10', '1');
INSERT INTO `sys_codelist` VALUES ('UNIT_TYPE', 'org', '机构', '', '20', '1');
INSERT INTO `sys_codelist` VALUES ('UNIT_TYPE', 'post', '岗位', '', '30', '1');
INSERT INTO `sys_codelist` VALUES ('USER_SEX', 'F', '女', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('USER_SEX', 'M', '男', 'null', '1', '1');

-- ----------------------------
-- Table structure for sys_codetype
-- ----------------------------
DROP TABLE IF EXISTS `sys_codetype`;
CREATE TABLE `sys_codetype` (
  `TYPE_ID` varchar(32) NOT NULL,
  `TYPE_NAME` varchar(32) DEFAULT NULL,
  `TYPE_GROUP` varchar(32) DEFAULT NULL,
  `TYPE_DESC` varchar(128) DEFAULT NULL,
  `IS_CACHED` char(1) DEFAULT NULL,
  `IS_UNITEADMIN` char(1) DEFAULT NULL,
  `IS_EDITABLE` char(1) DEFAULT NULL,
  `LEGNTT_LIMIT` varchar(6) DEFAULT NULL,
  `CHARACTER_LIMIT` char(1) DEFAULT NULL,
  `EXTEND_SQL` char(1) DEFAULT NULL,
  `SQL_BODY` varchar(512) DEFAULT NULL,
  `SQL_COND` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`TYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_codetype
-- ----------------------------
INSERT INTO `sys_codetype` VALUES ('AuthedHandlerId', '认证Handler定义', 'sys_code_define', '', 'Y', 'Y', 'Y', '', 'B', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('BOOL_DEFINE', '布尔定义', 'sys_code_define', '', 'Y', 'Y', 'Y', '1', 'C', '', '', '');
INSERT INTO `sys_codetype` VALUES ('CODE_TYPE_GROUP', '编码类型分组', 'app_code_define', '编码类型分组', null, null, null, null, null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('FUNCTION_TYPE', '功能类型', 'sys_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('HANDLER_TYPE', '控制器类型', 'sys_code_define', '', 'N', 'Y', 'Y', '32', 'C', null, null, null);
INSERT INTO `sys_codetype` VALUES ('INFO_TYPE', '信息类别', 'app_code_define', '', 'Y', 'Y', 'Y', '', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('MENUTREE_CASCADE', '是否展开', 'sys_code_define', '', 'Y', 'Y', 'Y', '1', 'N', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('OPER_CTR_TYPE', '操作控制类型', 'sys_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('ORGNAZITION_RANK', '组织级别', 'app_code_define', null, 'Y', 'Y', 'Y', null, null, 'N', null, null);
INSERT INTO `sys_codetype` VALUES ('ORGNAZITION_TYPE', '组织类别', 'app_code_define', '', 'Y', 'Y', 'Y', '', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('POSITION_TYPE', '岗位类型', 'app_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('RES_TYPE', '资源类型', 'sys_code_define', '', 'N', 'Y', 'Y', '16', 'C', null, null, null);
INSERT INTO `sys_codetype` VALUES ('SYS_VALID_TYPE', '有效标识符', 'app_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('UNIT_TYPE', '单位类型', 'app_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('USER_SEX', '性别类型', 'sys_code_define', '', 'N', 'Y', 'Y', '16', 'C', null, null, null);

-- ----------------------------
-- Table structure for sys_function
-- ----------------------------
DROP TABLE IF EXISTS `sys_function`;
CREATE TABLE `sys_function` (
  `FUNC_ID` varchar(36) NOT NULL,
  `FUNC_NAME` varchar(64) DEFAULT NULL,
  `FUNC_TYPE` varchar(32) DEFAULT NULL,
  `MAIN_HANDLER` varchar(36) DEFAULT NULL,
  `FUNC_PID` varchar(36) DEFAULT NULL,
  `FUNC_STATE` char(1) DEFAULT NULL,
  `FUNC_SORT` int(11) DEFAULT NULL,
  `FUNC_DESC` varchar(256) DEFAULT NULL,
  `FUNC_ICON` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`FUNC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_function
-- ----------------------------
INSERT INTO `sys_function` VALUES ('00000000-0000-0000-00000000000000000', '业务管理系统', 'funcmenu', null, null, '1', null, null, null);
INSERT INTO `sys_function` VALUES ('00000000-0000-0000-00000000000000001', '系统管理', 'funcmenu', '', '00000000-0000-0000-00000000000000000', '1', '99', '', null);
INSERT INTO `sys_function` VALUES ('0855A94F-F8E0-42D6-A0DF-72F1D11C6D0B', '投票管理', 'funcnode', '87D4B614-DF7E-48D2-BA04-3A31728740D9', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477', '1', '3', '', null);
INSERT INTO `sys_function` VALUES ('2F0F133F-1A6F-4E01-AADD-5B11C97BE6A0', '资源管理', 'funcnode', '6479AF01-D00B-4A40-B502-98FAF8418E92', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477', '1', '5', '', null);
INSERT INTO `sys_function` VALUES ('36954650-E79A-4682-8AC2-D3A78461A3D4', '资源分组', 'funcnode', '0543DD98-8AE6-4DF1-990E-24CD51E73564', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477', '1', '4', '', null);
INSERT INTO `sys_function` VALUES ('5FDEE3AB-6D32-4C5F-AD65-EF1EE5FFBAE6', '附件管理', 'funcnode', '1F617665-FC8B-4E8C-ABE4-540C363A17A8', '00000000-0000-0000-00000000000000001', '1', '8', '', null);
INSERT INTO `sys_function` VALUES ('67BA273A-DD31-48D0-B78C-1D60D5316074', '系统日志', 'funcnode', '494DF09B-7573-4CCA-85C1-97F4DC58C86B', '00000000-0000-0000-00000000000000001', '1', '7', null, null);
INSERT INTO `sys_function` VALUES ('692B0D37-2E66-4E82-92B4-E59BCF76EE76', '编码管理', 'funcnode', 'B4FE5722-9EA6-47D8-8770-D999A3F6A354', '00000000-0000-0000-00000000000000001', '1', '6', null, null);
INSERT INTO `sys_function` VALUES ('72D2FDBA-6400-4C48-A462-BA5E5B996631', '账户查询', 'funcnode', '6BA56996-4F31-464F-B230-A7E764CCC547', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477', '1', '2', '', null);
INSERT INTO `sys_function` VALUES ('8C84B439-2788-4608-89C4-8F5AA076D124', '组织机构', 'funcnode', '439949F0-C6B7-49FF-8ED1-2A1B5062E7B9', '00000000-0000-0000-00000000000000001', '1', '1', null, null);
INSERT INTO `sys_function` VALUES ('A0334956-426E-4E49-831B-EB00E37285FD', '编码类型', 'funcnode', '9A16D554-F989-438A-B92D-C8C8AC6BF9B8', '00000000-0000-0000-00000000000000001', '1', '5', null, null);
INSERT INTO `sys_function` VALUES ('AD5188DB-BF4F-4CFB-853E-8CA469D3E477', '样例功能', 'funcmenu', '', '00000000-0000-0000-00000000000000000', '1', '100', '', null);
INSERT INTO `sys_function` VALUES ('C843A0B6-2837-48A6-9C16-DCB6FFB37790', '账户管理', 'funcnode', '048E2C48-1BD4-4EB2-94E4-208835D302EB', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477', '1', '1', '', null);
INSERT INTO `sys_function` VALUES ('C977BC31-C78F-4B16-B0C6-769783E46A06', '功能管理', 'funcnode', '46C52D33-8797-4251-951F-F7CA23C76BD7', '00000000-0000-0000-00000000000000001', '1', '4', null, null);
INSERT INTO `sys_function` VALUES ('D3582A2A-3173-4F92-B1AD-2F999A2CBE18', '修改密码', 'funcnode', '88882DB9-967F-430E-BA9C-D0BBBBD2BD0C', '00000000-0000-0000-00000000000000001', '1', '9', '', null);
INSERT INTO `sys_function` VALUES ('DFE8BE4C-4024-4A7B-8DF2-630003832AE9', '角色管理', 'funcnode', '0CE03AD4-FF29-4FDB-8FEE-DA8AA38B649F', '00000000-0000-0000-00000000000000001', '1', '2', null, null);
INSERT INTO `sys_function` VALUES ('E5DD18E9-1CF0-4BAA-9A29-03EC56441A2A', '人员管理', 'funcnode', '1FE570C2-56C2-4370-8E0A-AC68FEAAD5B7', '00000000-0000-0000-00000000000000001', '1', '3', '', null);

-- ----------------------------
-- Table structure for sys_handler
-- ----------------------------
DROP TABLE IF EXISTS `sys_handler`;
CREATE TABLE `sys_handler` (
  `HANLER_ID` varchar(36) NOT NULL,
  `HANLER_CODE` varchar(64) DEFAULT NULL,
  `HANLER_TYPE` varchar(32) DEFAULT NULL,
  `HANLER_URL` varchar(128) DEFAULT NULL,
  `FUNC_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`HANLER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_handler
-- ----------------------------
INSERT INTO `sys_handler` VALUES ('048E2C48-1BD4-4EB2-94E4-208835D302EB', 'AccountManageList', 'MAIN', null, 'C843A0B6-2837-48A6-9C16-DCB6FFB37790');
INSERT INTO `sys_handler` VALUES ('0543DD98-8AE6-4DF1-990E-24CD51E73564', 'ResGroupTreeManage', 'MAIN', null, '36954650-E79A-4682-8AC2-D3A78461A3D4');
INSERT INTO `sys_handler` VALUES ('0CE03AD4-FF29-4FDB-8FEE-DA8AA38B649F', 'SecurityRoleTreeManage', 'MAIN', '', 'DFE8BE4C-4024-4A7B-8DF2-630003832AE9');
INSERT INTO `sys_handler` VALUES ('1157E31B-DA00-406C-BFCF-3BF5B588A5E0', 'SecurityUserManagerTreePick', 'OTHER', '', 'E5DD18E9-1CF0-4BAA-9A29-03EC56441A2A');
INSERT INTO `sys_handler` VALUES ('1D046723-D171-4FAE-BF4E-B1D26483E3E2', 'SecurityUserRGManageList', 'OTHER', '', 'E5DD18E9-1CF0-4BAA-9A29-03EC56441A2A');
INSERT INTO `sys_handler` VALUES ('1F617665-FC8B-4E8C-ABE4-540C363A17A8', 'WcmGeneralGroup8ContentList', 'MAIN', null, '5FDEE3AB-6D32-4C5F-AD65-EF1EE5FFBAE6');
INSERT INTO `sys_handler` VALUES ('1FE570C2-56C2-4370-8E0A-AC68FEAAD5B7', 'SecurityUserList', 'MAIN', null, 'E5DD18E9-1CF0-4BAA-9A29-03EC56441A2A');
INSERT INTO `sys_handler` VALUES ('439949F0-C6B7-49FF-8ED1-2A1B5062E7B9', 'SecurityGroupList', 'MAIN', '', '8C84B439-2788-4608-89C4-8F5AA076D124');
INSERT INTO `sys_handler` VALUES ('46C52D33-8797-4251-951F-F7CA23C76BD7', 'FunctionTreeManage', 'MAIN', '', 'C977BC31-C78F-4B16-B0C6-769783E46A06');
INSERT INTO `sys_handler` VALUES ('494DF09B-7573-4CCA-85C1-97F4DC58C86B', 'SysLogQueryList', 'MAIN', null, '67BA273A-DD31-48D0-B78C-1D60D5316074');
INSERT INTO `sys_handler` VALUES ('6479AF01-D00B-4A40-B502-98FAF8418E92', 'ResGroup8ContentList', 'MAIN', null, '2F0F133F-1A6F-4E01-AADD-5B11C97BE6A0');
INSERT INTO `sys_handler` VALUES ('6BA56996-4F31-464F-B230-A7E764CCC547', 'AccountQueryList', 'MAIN', null, '72D2FDBA-6400-4C48-A462-BA5E5B996631');
INSERT INTO `sys_handler` VALUES ('87D4B614-DF7E-48D2-BA04-3A31728740D9', 'VoteInfoManageList', 'MAIN', null, '0855A94F-F8E0-42D6-A0DF-72F1D11C6D0B');
INSERT INTO `sys_handler` VALUES ('88882DB9-967F-430E-BA9C-D0BBBBD2BD0C', 'ModifyPassword', 'MAIN', null, 'D3582A2A-3173-4F92-B1AD-2F999A2CBE18');
INSERT INTO `sys_handler` VALUES ('946CADBA-3007-4682-84DF-794BA0B2B4C6', 'SecurityUserQuery', 'OTHER', '', 'E5DD18E9-1CF0-4BAA-9A29-03EC56441A2A');
INSERT INTO `sys_handler` VALUES ('9A16D554-F989-438A-B92D-C8C8AC6BF9B8', 'CodeTypeManageList', 'MAIN', null, 'A0334956-426E-4E49-831B-EB00E37285FD');
INSERT INTO `sys_handler` VALUES ('B4FE5722-9EA6-47D8-8770-D999A3F6A354', 'CodeListManageList', 'MAIN', null, '692B0D37-2E66-4E82-92B4-E59BCF76EE76');

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `ID` char(36) DEFAULT NULL,
  `OPER_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IP_ADDTRESS` varchar(32) DEFAULT NULL,
  `USER_ID` varchar(32) DEFAULT NULL,
  `USER_NAME` varchar(32) DEFAULT NULL,
  `FUNC_NAME` varchar(64) DEFAULT NULL,
  `ACTION_TYPE` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_onlinecount
-- ----------------------------
DROP TABLE IF EXISTS `sys_onlinecount`;
CREATE TABLE `sys_onlinecount` (
  `IPADDRRESS` varchar(64) NOT NULL,
  `ONLINECOUNT` int(11) DEFAULT NULL,
  PRIMARY KEY (`IPADDRRESS`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_onlinecount
-- ----------------------------

-- ----------------------------
-- Table structure for sys_operation
-- ----------------------------
DROP TABLE IF EXISTS `sys_operation`;
CREATE TABLE `sys_operation` (
  `OPER_ID` char(36) NOT NULL,
  `HANLER_ID` varchar(36) DEFAULT NULL,
  `OPER_CODE` varchar(64) DEFAULT NULL,
  `OPER_NAME` varchar(64) DEFAULT NULL,
  `OPER_ACTIONTPYE` varchar(64) DEFAULT NULL,
  `OPER_SORT` int(11) DEFAULT NULL,
  PRIMARY KEY (`OPER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_operation
-- ----------------------------
INSERT INTO `sys_operation` VALUES ('3EC41FB8-ED98-41F3-B86D-F7473AEDD002', '494DF09B-7573-4CCA-85C1-97F4DC58C86B', 'viewDetail', '查看', 'viewDetail', '1');
INSERT INTO `sys_operation` VALUES ('6BF7A157-D333-4F40-8612-F61D3FD4D258', '494DF09B-7573-4CCA-85C1-97F4DC58C86B', 'refreshImgBtn', '刷新', 'refresh', '2');

-- ----------------------------
-- Table structure for wcm_general_group
-- ----------------------------
DROP TABLE IF EXISTS `wcm_general_group`;
CREATE TABLE `wcm_general_group` (
  `GRP_ID` varchar(36) NOT NULL,
  `GRP_NAME` varchar(64) DEFAULT NULL,
  `GRP_PID` varchar(36) DEFAULT NULL,
  `GRP_ORDERNO` int(11) DEFAULT NULL,
  `GRP_IS_SYSTEM` varchar(32) DEFAULT NULL,
  `GRP_RES_TYPE_DESC` varchar(32) DEFAULT NULL,
  `GRP_RES_TYPE_EXTS` varchar(128) DEFAULT NULL,
  `GRP_RES_SIZE_LIMIT` varchar(32) DEFAULT NULL,
  `GRP_DESC` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`GRP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wcm_general_group
-- ----------------------------
INSERT INTO `wcm_general_group` VALUES ('77777777-7777-7777-7777-777777777777', '附件目录', '', null, '', '', '', '', '');
INSERT INTO `wcm_general_group` VALUES ('A6018D88-8345-46EE-A452-CE362FAC72E2', '视频文件', '77777777-7777-7777-7777-777777777777', '1', 'Y', '视频', '*.mp4;*.3gp;*.wmv;*.avi;*.rm;*.rmvb;*.flv', '100MB', '');
INSERT INTO `wcm_general_group` VALUES ('CF35D1E6-102E-428A-B39C-0072D491D5B1', '业务附件', '77777777-7777-7777-7777-777777777777', '2', 'Y', '所有资源', '*.*', '2MB', '');

-- ----------------------------
-- Table structure for wcm_general_resource
-- ----------------------------
DROP TABLE IF EXISTS `wcm_general_resource`;
CREATE TABLE `wcm_general_resource` (
  `RES_ID` varchar(36) NOT NULL,
  `GRP_ID` varchar(36) DEFAULT NULL,
  `RES_NAME` varchar(64) DEFAULT NULL,
  `RES_SHAREABLE` varchar(32) DEFAULT NULL,
  `RES_LOCATION` varchar(256) DEFAULT NULL,
  `RES_SIZE` varchar(64) DEFAULT NULL,
  `RES_SUFFIX` varchar(32) DEFAULT NULL,
  `RES_DESCRIPTION` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`RES_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wcm_general_resource
-- ----------------------------
