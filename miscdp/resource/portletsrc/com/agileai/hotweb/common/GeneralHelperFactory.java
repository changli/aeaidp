package com.agileai.hotweb.common;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import com.agileai.portal.driver.core.IUserCacheManager;
import com.agileai.portal.driver.servlet.ParamInteractiveServlet;

public class GeneralHelperFactory {
	
	public static IUserCacheManager getUserCacheManager(HttpServletRequest request){
		ServletContext currentContext = request.getServletContext();
		ServletContext context = currentContext.getContext(ParamInteractiveServlet.DefaultPortalContext);
		IUserCacheManager userCacheManager = (IUserCacheManager)context.getAttribute(IUserCacheManager.class.getName());
		return userCacheManager;
	}
	
}
