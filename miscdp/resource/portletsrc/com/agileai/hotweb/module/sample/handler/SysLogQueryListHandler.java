package com.agileai.hotweb.module.sample.handler;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.QueryModelListHandler;
import com.agileai.hotweb.module.sample.service.SysLogQuery;

public class SysLogQueryListHandler
        extends QueryModelListHandler {
    public SysLogQueryListHandler() {
        super();
        this.detailHandlerClazz = SysLogQueryDetailHandler.class;
        this.serviceId = buildServiceId(SysLogQuery.class);
    }

    protected void processPageAttributes(DataParam param) {
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "sdate", "");
        initParamItem(param, "edate", "");
    }

    protected SysLogQuery getService() {
        return (SysLogQuery) this.lookupService(this.getServiceId());
    }
}
