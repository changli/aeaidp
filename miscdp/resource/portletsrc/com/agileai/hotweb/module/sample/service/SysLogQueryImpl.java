package com.agileai.hotweb.module.sample.service;

import com.agileai.hotweb.bizmoduler.core.QueryModelServiceImpl;
import com.agileai.hotweb.cxmodule.SampleShareInterface;

public class SysLogQueryImpl
        extends QueryModelServiceImpl
        implements SysLogQuery ,SampleShareInterface{
	
    public SysLogQueryImpl() {
        super();
    }

	@Override
	public String provideSharedData() {
		return "";
	}
}
