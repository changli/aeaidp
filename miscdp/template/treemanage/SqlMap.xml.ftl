<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE sqlMap      
    PUBLIC "-//ibatis.apache.org//DTD SQL Map 2.0//EN"      
    "http://ibatis.apache.org/dtd/sql-map-2.dtd">
<sqlMap namespace="${table.namespace}">  
  <select id="queryTreeRecords" parameterClass="com.agileai.domain.DataParam" resultClass="com.agileai.domain.DataRow">
	${table.queryTreeRecordsSql}
  </select>
  <select id="queryCurLevelRecords" parameterClass="String" resultClass="com.agileai.domain.DataRow">
    ${table.queryCurLevelRecordsSql}
  </select>
  <select id="queryChildRecords" parameterClass="String" resultClass="com.agileai.domain.DataRow">
    ${table.queryChildRecordsSql}
  </select>  
  <select id="queryTreeRecord" parameterClass="com.agileai.domain.DataParam" resultClass="com.agileai.domain.DataRow">
    ${table.queryTreeRecordSql}
  </select>
  <select id="queryMaxSortId" parameterClass="String" resultClass="com.agileai.domain.DataRow">
    ${table.queryMaxSortIdSql}
  </select>     
  <insert id="insertTreeRecord" parameterClass="com.agileai.domain.DataParam">
    ${table.insertTreeRecordSql}
  </insert>
  <update id="updateTreeRecord" parameterClass="com.agileai.domain.DataParam">
    ${table.updateTreeRecordSql}
  </update>
  <delete id="deleteTreeRecord" parameterClass="String">
    ${table.deleteTreeRecordSql}
  </delete>
</sqlMap>