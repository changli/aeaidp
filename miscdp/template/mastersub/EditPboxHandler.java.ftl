<#ftl ns_prefixes={"D":"http://www.hotweb.agileai.com/model/mastersub",
"fa":"http://www.hotweb.agileai.com/model"}>
<#import "/common/Util.ftl" as Util>
<#import "/common/PageForm.ftl" as Form>
<#visit doc>
<#macro MasterSubFuncModel>
<#local baseInfo = .node.BaseInfo>
<#local handler = baseInfo.Handler>
<#local service = baseInfo.Service>
<#local editPboxView = .node.EditPboxView[currentEntryEditTableIndex]>
<#local boxEditArea = .node.EditPboxView[currentEntryEditTableIndex].SubPboxEditArea>
package ${Util.parsePkg(editPboxView.@handlerClass)};

import java.util.*;

import ${service.@InterfaceName};
import com.agileai.hotweb.controller.core.MasterSubEditPboxHandler;
import com.agileai.domain.*;
import com.agileai.hotweb.domain.*;
import com.agileai.util.*;

public class ${Util.parseClass(editPboxView.@handlerClass)} extends MasterSubEditPboxHandler{
	public ${Util.parseClass(editPboxView.@handlerClass)}(){
		super();
		this.serviceId = buildServiceId(${Util.parseClass(service.@InterfaceName)}.class);
		this.subTableId = "${editPboxView.@subTableId}";
	}
	protected void processPageAttributes(DataParam param) {
		if (!StringUtil.isNullOrEmpty(param.get("${baseInfo.@tablePK}"))){
			this.setAttribute("${baseInfo.@tablePK}", param.get("${baseInfo.@tablePK}"));
		}
		<#compress>
		<#if Util.isValid(boxEditArea)><#list boxEditArea["fa:FormObject"] as formObject><@SetAttribute formAtom=formObject/>
		</#list></#if>
		</#compress>
		
	}
	protected ${Util.parseClass(service.@InterfaceName)} getService() {
		return (${Util.parseClass(service.@InterfaceName)})this.lookupService(this.getServiceId());
	}
}
</#macro>

<#macro SetAttribute formAtom><#local type=formAtom.*[0]?node_name?lower_case><#local atom = formAtom.*[0]><#if (type="select" || type="radio" || type="checkbox")>
	setAttribute("${atom["fa:Name"]}",FormSelectFactory.create("${atom["fa:ValueProvider"]}").addSelectedValue(getOperaAttributeValue("${atom["fa:Name"]}","${atom["fa:DefValue"].@value}")));</#if>
</#macro>